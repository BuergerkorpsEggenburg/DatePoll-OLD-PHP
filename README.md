# DatePoll
Termine einfach schneller finden: Verkürze mit dem Online-Terminplaner DatePoll radikal den Prozess der Terminabstimmung für Personengruppen. Völlig kostenlos!

## Installation
1) Lade dir einfach die Dateien herunter, schieb sie auf deinen Webserver und click dich durch das Setup.
2) Erzähle deinen Freunden davon und lege einen Termin an.
3) Werte die Abstimmung aus!
