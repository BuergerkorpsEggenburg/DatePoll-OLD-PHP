<?php
require_once('../../logic/parser.php');
require_once('../../logic/userHandler.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}
$birthdaysOfTheMonth = genBirthdaysOfTheMonth();
?>
	<div class="card-deck">
		<h1 class="w-100 text-center">Hallo <?php echo $_SESSION['firstname'] . " " . $_SESSION['surname']; ?>!</h1>

		<div class="col-md-6">
			<div class="card">
				<div class="card-header" style="text-align: center;">
					unbeantwortete Abstimmungen
				</div>
				<div class="card-body text-center">
					<?php
					require_once('../../logic/pollHandler.php');
					$polls = listOpenPolls();
					if (count($polls) == 0) {
						echo parseAlert('<strong>Du hast keine unbeantworteten Abstimmungen! <br /> Weiter so!</strong>', 'success', false);
					} else {
						$table = parseTableHead(parseTableData('Titel') . parseTableData('Datum'));
						foreach ($polls as $poll) {
							$detail = ' onclick="load_page(\'sites/poll/vote/vote.php\')"';                //lade Abstimmungsseite für pollID = n
							$row = "";

							$row .= parseTableData($poll['title'], $detail);
							$row .= parseTableData(date("d.m.Y H:i", strtotime($poll['finishDate'])), $detail);

							$row = parseTableRow($row);
							$table .= $row;
						}
						$table = parseTable($table, 'class="table table-bordered table-hover"');
						echo $table;
					}
					?>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<?php echo $birthdaysOfTheMonth; ?>
		</div>

		<div id="modalContainer"></div>
	</div>
<?php

function genBirthdaysOfTheMonth() {
	$users = getBirthdayOfTheMonth();
	if (count($users) == 0) {
		return parseCard(parseCardHeader("Geburtstage des Monats")
				, parseAlert("<b>Schade...</b><br />Dieses Monat hat niemand Geburtstag...", 'warning', false)
				, '', 'text-center');
	} else {
		$card = parseTableHead(parseTableData('Name') . parseTableData('Geburtstag'));
		foreach ($users as $user) {
			$card .= parseTableRow(parseTableData($user['firstname'] . ' ' . $user['surname']) .
					parseTableData(date("d.m.Y", strtotime($user['birthdate']))),
					'onclick="showProfileOf(' . $user['userID'] . ')"'
			);
		}
		$card = parseTable($card, 'class="table table-bordered"');
		return parseCard(parseCardHeader("Geburtstage des Monats"),
				$card, '', 'text-center');
	}
}