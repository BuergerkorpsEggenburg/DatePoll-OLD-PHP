<?php
require_once('../../logic/userHandler.php');

if (!canAccessBasicPage()) {
  echo false;
  die();
}
if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
  die();
}

require_once('../../logic/groupHandler.php');
require_once('../../logic/pollHandler.php');
require_once('../../logic/parser.php');
?>
<div class="card-deck">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <i class="fas fa-calendar-check"></i> Abstimmungen

        <span data-toggle="collapse" data-target="#collapseAddPoll" aria-expanded="false" aria-controls="collapseAddPoll">
          <button id="showAddPollDivButton" class="btn btn-outline-success float-right" onclick="toggleAddPollDiv()"
                  data-toggle="tooltip" data-placement="bottom" title="Öffne das Abstimmungerstellfenster">
            <i class="fas fa-plus"></i>
          </button>
        </span>
        <span data-toggle="collapse" data-target="#collapseAddPoll" aria-expanded="false" aria-controls="collapseAddPoll">
          <button id="hideAddPollDivButton" hidden class="btn btn-outline-warning float-right" onclick="toggleAddPollDiv()"
                  data-toggle="tooltip" data-placement="bottom" title="Schließe das Abstimmungerstellfenster">
            <i class="fas fa-times"></i>
          </button>
        </span>
        <button id="addPollButton" hidden class="btn btn-success float-right" onclick="CreatePoll()" style="margin-right: 5px;"
                data-toggle="tooltip" data-placement="bottom" title="Erstelle diese Abstimmung">
          Erstellen
        </button>
      </div>
      <div class="card-body">
        <div class="collapse" id="collapseAddPoll" style="margin-bottom: 10px;">
          <div class="card card-body">
            <div class="row" style="padding-left: 15px;">
              <p>Füge eine neues Abstimmung hinzu:</p>
            </div>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-8">
                <div class="form-group">
                  <label for="inputTitel">Titel: *</label>
                  <div class="input-group" id="inputGroupTitel">
                    <div class="input-group-prepend">
                      <label for="inputTitel" class="input-group-text"><i class="fas fa-tag"></i></label>
                    </div>
                    <input type="text" class="form-control" id="inputTitel" name="inputTitel" required autofocus
                           placeholder="Titel der Abstimmung" oninput="CreatePollInputChange()">
                  </div>
                  <div class="row" style="margin-left: 1px;">
                    <small id="titleIncorrect" hidden class="form-text" style="color: red">
                      Der Title muss größer gleich <strong>1</strong> Zeichen und kleiner gleich <strong>64</strong> Zeichen haben.
                    </small>
                  </div>
                  <div class="row" style="margin-left: 1px;">
                    <small class="form-text">
                      Alle Felder mit * sind pflicht!
                    </small>
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="form-group">
                  <label for="inputFinishDate">Enddatum: *</label>
                  <div class="input-group" id="inputGroupFinishDate">
                    <div class="input-group-prepend">
                      <label for="inputFinishDate" class="input-group-text"><i class="fas fa-calendar"></i></label>
                    </div>
                    <input type="date" class="form-control" id="inputFinishDate" name="inputFinishDate" oninput="CreatePollInputChange()">
                  </div>
                  <div class="row" style="margin-left: 1px;">
                    <small id="finishDateIncorrect" hidden class="form-text" style="color: red">
                      Das Datum ist nicht korrekt!
                    </small>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="form-group">
                  <label for="inputDescription">Beschreibung: </label>
                  <div class="input-group" id="inputGroupDescription">
                    <div class="input-group-prepend">
                      <label for="inputDescription" class="input-group-text"><i class="fas fa-database"></i></label>
                    </div>
                    <textarea id="inputDescription" name="inputDescription" class="form-control"
                              oninput="this.style.height='24px'; this.style.height = this.scrollHeight + 12 + 'px';"
                              placeholder="Beschreibung der Abstimmung"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <div class="row">
                  <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                    <button class="btn btn-success" data-toggle="tooltip" data-placement="left"
                            title="Füge eine neue Abteilung für diese Abstimmung hinzu"
                            onclick="CreatePollAddGroupToPoll()" style="margin-right: 5px;">
                      <i class="fas fa-plus-circle"></i>
                    </button>
                  </div>
                  <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                    <select class="custom-select" id="groupToPollSelect" title="groupToPollS">
                      <option value="selectGroup" selected>Wähle eine Abteilung welche zur Abstimmung hinzugefügt werden soll</option>
                      <?php
                      $groups = getGroups();
                      foreach ($groups as $group) {
                        echo '<option value="' . $group['groupID'] . '" data-name="' . $group['name'] . '">' . $group['name'] . '</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="row" style="margin-left: 0.05vh; margin-top: 1vh">
                  <?php
                  $table = parseTableHead(parseTableData('Abteilungen') . parseTableData('Löschen'));
                  $table = parseTable($table, 'id="groupTable" class="table table-bordered table-hover"');

                  echo $table;
                  ?>
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <div class="row">
                  <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                    <button class="btn btn-success" data-toggle="tooltip" data-placement="left"
                            title="Füge eine neue Gruppe für diese Abstimmung hinzu"
                            onclick="CreatePollAddSubGroupToPoll()" style="margin-right: 5px;">
                      <i class="fas fa-plus-circle"></i>
                    </button>
                  </div>
                  <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                    <select class="custom-select" id="subGroupToPollSelect" title="subGroupToPollS">
                      <option value="selectSubGroup" selected>Wähle eine Gruppe welche zur Abstimmung hinzugefügt werden soll</option>
                      <?php
                      $subgroups = getSubGroups();
                      foreach ($subgroups as $subgroup) {
                        echo '<option value="' . $subgroup['groupID'] . '" data-name="' . $subgroup['name'] . '">' . $subgroup['name'] . '</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="row" style="margin-left: 0.05vh; margin-top: 1vh">
                  <?php
                  $table = parseTableHead(parseTableData('Gruppen') . parseTableData('Löschen'));
                  $table = parseTable($table, 'id="subGroupTable" class="table table-bordered table-hover"');

                  echo $table;
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>


        <?php
        $table = parseTableHead(parseTableData('Name') .
          parseTableData('Beschreibung') .
          parseTableData('Enddatum') .
          parseTableData('Abteilungen') .
          parseTableData('Gruppen') .
          parseTableData("Editieren") .
          parseTableData("Löschen"));
        $polls = getPolls();
        foreach ($polls as $poll) {
          $btnEdit = parseTag("button", parseTag("i", "", "class='fas fa-edit'"),
            "class = 'btn btn-primary' onclick='getPollDetails(" . $poll['pollID'] . ", true)'");
          $btnDelete = parseTag("button", parseTag("i", "", "class='fas fa-trash'"),
            'class = "btn btn-danger" onclick="deletePoll(' . $poll['pollID'] . ',\'' . $poll['title'] .'\')"');

          $detail = 'onclick="getPollDetails(' . $poll['pollID'] . ')"';

          $groupColumn = "";
          $groups = getGroupAssignedToPoll($poll['pollID']);
          foreach ($groups as $group) {
            $groupColumn .= $group['name'] . ', ';
          }

          $subGroupColumn = "";
          $subGroups = getSubGroupAssignedToPoll($poll['pollID']);
          foreach ($subGroups as $subGroup) {
            $subGroupColumn .= $subGroup['name'] . ', ';
          }

          $descriptionLength = strlen ($poll['description']);

          $description = $poll['description'];

          if($descriptionLength > 30) {
            $description = substr($description,0,30).'...';
          }

          $row = '';

          $row .= parseTableData($poll['title'], $detail);
          $row .= parseTableData($description, $detail);
          $row .= parseTableData(date("d.m.Y", strtotime($poll['finishDate'])), $detail);

          $row .= parseTableData($groupColumn , $detail);
          $row .= parseTableData($subGroupColumn, $detail);

          $row .= parseTableData($btnEdit);
          $row .= parseTableData($btnDelete);

          $row = parseTableRow($row);
          $table .= $row;
        }
        $table = parseTable($table, 'class="table table-bordered table-hover table-responsive"');

        echo $table;
        ?>
      </div>
    </div>
  </div>
</div>