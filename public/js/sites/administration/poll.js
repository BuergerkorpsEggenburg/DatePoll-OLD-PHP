let CreatePollGroups = [];
let CreatePollSubGroups = [];

/**
 * Opens the Add poll div collapse
 */
function toggleAddPollDiv() {
  let showAddPollDivButton = document.getElementById('showAddPollDivButton');
  let hideAddPollDivButton = document.getElementById("hideAddPollDivButton");

  if(showAddPollDivButton.hidden === true ) {
    unhide(showAddPollDivButton);
    hide(hideAddPollDivButton);

    CreatePollInputChange();
  } else {
    hide(showAddPollDivButton);
    unhide(hideAddPollDivButton);
  }
}

/**
 * Adds a group in the poll create div
 */
function CreatePollAddGroupToPoll() {
  //Wait until the animation is finished
  setTimeout(function () {
    clearTooltips();
  }, 600);

  let yourSelect = document.getElementById("groupToPollSelect");

  let selected = yourSelect.options[yourSelect.selectedIndex];
  if (selected.value === 'selectGroup') {
    showErrorAlert("Bitte wähle eine Abteilung aus!");
  } else {
    for (let i = 0; i < CreatePollGroups.length; i++) {
      if (CreatePollGroups[i] === selected.value) {
        showErrorAlert("Diese Abteilung ist bereits verhanden!");
        return;
      }
    }

    yourSelect.remove(yourSelect.selectedIndex);

    let table = document.getElementById("groupTable");
    let row = table.insertRow(table.rows.length);
    let nameCell = row.insertCell(0);
    nameCell.innerHTML = selected.getAttribute("data-name");
    let actionCell = row.insertCell(1);
    actionCell.innerHTML = `<button class="btn btn-danger" data-toggle="tooltip" data-placement="right" 
                              title="Entferne diese Abteilung für die Abstimmung" data-id="${selected.value}" data-name="${selected.getAttribute("data-name")}"
                              onclick="CreatePollRemoveGroupFromPoll(this)"><i class="fas fa-trash-alt"></i></button>`;

    CreatePollGroups.push(selected.value);

    bounceIn(row);
    loadDependencies();
  }
}

/**
 * Removes a group in the poll create div
 * @param btn button button which is pressed
 */
function CreatePollRemoveGroupFromPoll(btn) {
  let row = btn.parentNode.parentNode;
  btn.onclick = null;
  bounceOut(row);

  let count = -1;

  for (let i = 0; i < CreatePollGroups.length; i++) {
    if (CreatePollGroups[i] === btn.getAttribute("data-id")) {
      count = i;
    }
  }

  CreatePollGroups[count] = "NONE";

  //Wait until the animation is finished
  setTimeout(function () {
    row.parentNode.removeChild(row);

    let name = btn.getAttribute("data-name");
    let groupID = btn.getAttribute("data-id");

    let yourSelect = document.getElementById("groupToPollSelect");
    let option = document.createElement("option");
    option.text = name;
    option.value = groupID;
    option.setAttribute("data-name", name);
    yourSelect.add(option);

    clearTooltips();
  }, 600);
}

/**
 * Adds a subgroup in the poll create div
 */
function CreatePollAddSubGroupToPoll() {
  //Wait until the animation is finished
  setTimeout(function () {
    clearTooltips();
  }, 600);

  let yourSelect = document.getElementById("subGroupToPollSelect");

  let selected = yourSelect.options[yourSelect.selectedIndex];
  if (selected.value === 'selectSubGroup') {
    showErrorAlert("Bitte wähle eine Gruppe aus!");
  } else {
    for (let i = 0; i < CreatePollSubGroups.length; i++) {
      if (CreatePollSubGroups[i] === selected.value) {
        showErrorAlert("Diese Gruppe ist bereits verhanden!");
        return;
      }
    }

    yourSelect.remove(yourSelect.selectedIndex);

    let table = document.getElementById("subGroupTable");
    let row = table.insertRow(table.rows.length);
    let nameCell = row.insertCell(0);
    nameCell.innerHTML = selected.getAttribute("data-name");
    let actionCell = row.insertCell(1);
    actionCell.innerHTML = '' +
      '<button class="btn btn-danger" data-toggle="tooltip" data-placement="right" ' +
      'title="Entferne diese Gruppe für die Abstimmung" data-id="' + selected.value + '" data-name="' + selected.getAttribute("data-name") +'"' +
      ' onclick="CreatePollRemoveSubGroupFromPoll(this)"><i class="fas fa-trash-alt"></i></button>';

    CreatePollSubGroups.push(selected.value);

    bounceIn(row);
    loadDependencies();
  }
}

/**
 * Removes a subgroup in the poll create div
 * @param btn button button which is pressed
 */
function CreatePollRemoveSubGroupFromPoll(btn) {
  let row = btn.parentNode.parentNode;
  btn.onclick = null;
  bounceOut(row);

  let count = -1;

  for (let i = 0; i < CreatePollSubGroups.length; i++) {
    if (CreatePollSubGroups[i] === btn.getAttribute("data-id")) {
      count = i;
    }
  }

  CreatePollSubGroups[count] = "NONE";

  //Wait until the animation is finished
  setTimeout(function () {
    row.parentNode.removeChild(row);

    let name = btn.getAttribute("data-name");
    let groupID = btn.getAttribute("data-id");

    let yourSelect = document.getElementById("subGroupToPollSelect");
    let option = document.createElement("option");
    option.text = name;
    option.value = groupID;
    option.setAttribute("data-name", name);
    yourSelect.add(option);

    clearTooltips();
  }, 600);
}

/**
 * Checks the inputs in the poll create div and validates it
 */
function CreatePollInputChange() {
  let inputTitle = document.getElementById("inputTitel");
  let inputFinishDate = document.getElementById("inputFinishDate");
  let addPollButton = document.getElementById("addPollButton");

  let titleIncorrect = document.getElementById("titleIncorrect");
  let finishDateIncorrect = document.getElementById("finishDateIncorrect");

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(titleIncorrect);

      xhr_post('../../../logic/validationHandler.php', function (xhttp_) {
        if (xhttp_.responseText === "1") {
          hide(finishDateIncorrect);

          unhide(addPollButton);
        } else {
          unhide(finishDateIncorrect);
          hide(addPollButton);
        }
      }, 'action=isValidDate&date=' + inputFinishDate.value);

    } else {
      unhide(titleIncorrect);
      hide(addPollButton);
    }
  }, 'action=isValidPollTitle&title=' + inputTitle.value);
}

/**
 * On button pressed create poll
 */
function CreatePoll() {
  let inputTitle = document.getElementById("inputTitel");
  let inputFinishDate = document.getElementById("inputFinishDate");
  let inputDescription = document.getElementById("inputDescription");

  let addPollPrivateGroups = [];
  for (let i = 0; i < CreatePollGroups.length; i++) {
    if (CreatePollGroups[i] !== "NONE") {
      addPollPrivateGroups.push(CreatePollGroups[i]);
    }
  }

  let addPollPrivateSubGroups = [];
  for (let i = 0; i < CreatePollSubGroups.length; i++) {
    if (CreatePollSubGroups[i] !== "NONE") {
      addPollPrivateSubGroups.push(CreatePollSubGroups[i]);
    }
  }

  xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) {

    switch (xhttp.responseText) {
      case 'notEnoughPermissions':
        alert("Keine Berechtigung");
        return;

      case 'valid_pollTitle':
        resetInput(inputTitle);
        shake(document.getElementById("inputGroupTitel"));

        return;
      case 'valid_date':
        resetInput(inputFinishDate);
        shake(document.getElementById("inputGroupFinishDate"));

        return;
      default:
        //Get Poll id
        let pollID = xhttp.responseText;

        for (let i = 0; i < addPollPrivateGroups.length; i++) {
          xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) { },
            'action=assignGroupToPoll&pollID=' + pollID + '&groupID=' + addPollPrivateGroups[i]);
        }

        for (let i = 0; i < addPollPrivateSubGroups.length; i++) {
          xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) { },
            'action=assignSubGroupToPoll&pollID=' + pollID + '&subGroupID=' + addPollPrivateSubGroups[i]);
        }

        showSuccessAlert("Die Abstimmung <strong>" + inputTitle.value + "</strong> wurde erfolgreich für den <strong>"
          + inputFinishDate.value + "</strong> eingetragen!");
        load_main_content('./sites/administration/poll.php');

        CreatePollGroups = [];
        CreatePollSubGroups = [];

        clearTooltips();

        break;
    }

  }, 'action=createPoll&title=' + inputTitle.value + '&description=' + inputDescription.value + '&finishDate=' + inputFinishDate.value);
}

/**
 * Deletes poll on button pressed
 * @param pollID int poll which should be removed
 * @param title string title of the poll
 */
function deletePoll(pollID, title) {
  xhr_post('../logic/pollHandler/administration.php', function (xhttp) {
    switch (xhttp.responseText) {
      case '1':
        showSuccessAlert("Die Abstimmung <strong>" + title + "</strong> wurde erfolgreich gelöscht!");

        break;
      case 'notEnoughPermissions':
        showErrorAlert("Du hast nicht genügend Rechte um die Abstimmung zu löschen!");

        break;
      default:
        showErrorAlert("Die Abstimmung <strong>" + title + "</strong> wurde nicht gelöscht!");

        break;
    }

    load_main_content_and_change_active('./sites/administration/poll.php', 'poll');
  }, 'action=deletePoll&pollID=' + pollID);
}


/**
 * gets the details of the given poll and loads it into the container with the id 'pollDetail'
 * @param pollID    pollID of the poll to get the details of
 * @param edit if edit is true the modal will be shown in edit mode
 */
function getPollDetails(pollID, edit = false) {
  let toExecute = function() {
    let myTA = $('#inputDetailDescription');
    let height = myTA[0].scrollHeight + 20;

    document.getElementById("inputDetailDescription").style.height = height + "px";

    if(edit) {
      switchInputsForPollDetails(document.getElementById("switchInputsForPollDetailsButton"));
    }
  };

  let toExecuteAfterModalClose = function() {
    load_main_content('./sites/administration/poll.php');
  };

  load_modal("sites/poll/detail.php?pollID=" + pollID, 'modalContainer', toExecute, toExecuteAfterModalClose);
}

/**
 * enables all inputs in poll detail modal
 */
function switchInputsForPollDetails() {

  let inputs = document.getElementsByClassName("detailPoll");

  let button = document.getElementById("switchInputsForPollDetailsButton");

  if(inputs[0].disabled) {
    button.classList.remove("btn-outline-primary");
    button.classList.add("btn-primary");

    for (let i = 0; i < inputs.length; i++) {
      enable(inputs[i]);
    }
  } else {
    button.classList.remove("btn-primary");
    button.classList.add("btn-outline-primary");

    for (let i = 0; i < inputs.length; i++) {
      disable(inputs[i]);
    }
  }

  setTimeout(function () {
    clearTooltips();
  }, 500);
}

/**
 * Adds a group in the poll edit modal
 */
function EditPollAddGroupToPoll() {
  let yourSelect = document.getElementById("detailGroupToPollSelect");

  let selected = yourSelect.options[yourSelect.selectedIndex];
  if (selected.value === 'selectGroup') {
    showModalErrorAlert("Bitte wähle eine Abteilung aus!");
  } else {
    yourSelect.remove(yourSelect.selectedIndex);

    let pollID = selected.getAttribute("data-pollid");
    let name = selected.getAttribute("data-name");
    let groupID = selected.value;

    let table = document.getElementById("detailGroupTable");
    let row = table.insertRow(table.rows.length);
    let nameCell = row.insertCell(0);
    nameCell.innerHTML = name;
    let actionCell = row.insertCell(1);
    actionCell.innerHTML = '' +
      '<button class="btn btn-danger detailPoll" data-toggle="tooltip" data-placement="right" title="Entferne diese Abteilung"' +
      ' onclick="EditPollRemoveGroupFromPoll(' + pollID + ', ' + groupID + ', \'' + name + '\', this)"><i class="fas fa-trash-alt"></i></button>';

    bounceIn(row);
    loadDependencies();

    xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) {

    },'action=assignGroupToPoll&groupID=' + groupID + '&pollID=' + pollID);
  }

  setTimeout(function () {
    clearTooltips();
  }, 500);
}

/**
 * Remove a group in the poll edit modal
 * @param pollID int poll which should be edited
 * @param groupID int the group which should be removed
 * @param name string name from the group
 * @param btn button pressed button
 */
function EditPollRemoveGroupFromPoll(pollID, groupID, name, btn) {
  xhr_post('../logic/pollHandler/administration.php', function (xhttp) {
    if(xhttp.responseText === "1") {
      let row = btn.parentNode.parentNode;
      btn.onclick = null;
      bounceOut(row);

      //Wait until the animation is finished
      setTimeout(function () {
        row.parentNode.removeChild(row);

        let yourSelect = document.getElementById("detailGroupToPollSelect");
        let option = document.createElement("option");
        option.text = name;
        option.value = groupID;
        option.setAttribute("data-name", name);
        option.setAttribute("data-pollid", pollID);
        yourSelect.add(option);

        clearTooltips();

        showModalSuccessAlert("Die Abteilung <strong>" + name + "</strong> wurde von dieser Abstimmung entfernt!");
      }, 600);

    } else {
      showModalErrorAlert("Du hast nicht genügend Rechte um die Abstimmung zu bearbeiten!");
    }

  }, 'action=unassignGroupFromPoll&pollID=' + pollID + '&groupID=' + groupID);
}

/**
 * Adds a subgroup in the poll edit modal
 */
function EditPollAddSubGroupToPoll() {
  let yourSelect = document.getElementById("detailSubGroupToPollSelect");

  let selected = yourSelect.options[yourSelect.selectedIndex];
  if (selected.value === 'selectSubGroup') {
    showModalErrorAlert("Bitte wähle eine Gruppe aus!");
  } else {
    yourSelect.remove(yourSelect.selectedIndex);

    let pollID = selected.getAttribute("data-pollid");
    let name = selected.getAttribute("data-name");
    let subGroupID = selected.value;

    let table = document.getElementById("detailSubGroupTable");
    let row = table.insertRow(table.rows.length);
    let nameCell = row.insertCell(0);
    nameCell.innerHTML = name;
    let actionCell = row.insertCell(1);
    actionCell.innerHTML = '' +
      '<button class="btn btn-danger detailPoll" data-toggle="tooltip" data-placement="right" title="Entferne diese Gruppe"' +
      ' onclick="EditPollRemoveSubGroupFromPoll(' + pollID + ', ' + subGroupID + ', \'' + name + '\', this)"><i class="fas fa-trash-alt"></i></button>';

    bounceIn(row);
    loadDependencies();

    xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) {

    },'action=assignSubGroupToPoll&subgroupID=' + subGroupID + '&pollID=' + pollID);
  }

  setTimeout(function () {
    clearTooltips();
  }, 500);
}

/**
 * Remove a subgroup in the poll edit modal
 * @param pollID int poll which should be edited
 * @param subGroupID int the subgroup which should be removed
 * @param name string name from the subgroup
 * @param btn button pressed button
 */
function EditPollRemoveSubGroupFromPoll(pollID, subGroupID, name, btn) {
  xhr_post('../logic/pollHandler/administration.php', function (xhttp) {
    if(xhttp.responseText === "1") {
      let row = btn.parentNode.parentNode;
      btn.onclick = null;
      bounceOut(row);

      //Wait until the animation is finished
      setTimeout(function () {
        row.parentNode.removeChild(row);

        let yourSelect = document.getElementById("detailSubGroupToPollSelect");
        let option = document.createElement("option");
        option.text = name;
        option.value = subGroupID;
        option.setAttribute("data-name", name);
        option.setAttribute("data-pollid", pollID);
        yourSelect.add(option);

        clearTooltips();

        showModalSuccessAlert("Die Gruppe <strong>" + name + "</strong> wurde von dieser Abstimmung entfernt!");
      }, 600);

    } else {
      showModalErrorAlert("Du hast nicht genügend Rechte um die Abstimmung zu bearbeiten!");
    }

  }, 'action=unassignSubGroupFromPoll&pollID=' + pollID + '&subGroupID=' + subGroupID);
}

/**
 * Checks the input in poll edit modal and validates it
 */
function EditPollInputChange() {
  let inputTitle = document.getElementById("inputDetailTitel");
  let inputDate = document.getElementById("inputDetailFinishDate");
  let inputDescription = document.getElementById("inputDetailDescription");

  let oldValueTitle = document.getElementById("inputDetailTitel_old").value;
  let oldValueDate = document.getElementById("inputDetailFinishDate_old").value;
  let oldValueDescription = document.getElementById("inputDetailDescription_old").value;

  let titleErrorField = document.getElementById("titleEditIncorrect");
  let dateErrorField = document.getElementById("finishDateEditIncorrect");
  let descriptionErrorField = document.getElementById("descriptionEditIncorrect");

  let pollDetailSaveButton = document.getElementById("pollDetailSaveButton");

  let haveValuesChanged = false;

  if(inputTitle.value !== oldValueTitle) {
    haveValuesChanged = true;

    xhr_post('../../../logic/validationHandler.php', function (xhttp) {
      if(xhttp.responseText === "1") {
        hide(titleErrorField);
      } else {
        unhide(titleErrorField);
      }
    },'action=isValidPollTitle&title=' + inputTitle.value);
  }

  if(inputDescription.value !== oldValueDescription) {
    haveValuesChanged = true;

    xhr_post('../../../logic/validationHandler.php', function (xhttp) {
      if(xhttp.responseText === "1") {
        hide(descriptionErrorField);
      } else {
        unhide(descriptionErrorField);
      }
    },'action=isValidPollDescription&description=' + inputDescription.value);
  }

  if(inputDate.value !== oldValueDate) {
    haveValuesChanged = true;

    xhr_post('../../../logic/validationHandler.php', function (xhttp) {
      if(xhttp.responseText === "1") {
        hide(dateErrorField);
      } else {
        unhide(dateErrorField);
      }
    },'action=isValidDate&date=' + inputDate.value);
  }

  if(haveValuesChanged) {
    unhide(pollDetailSaveButton);
  } else {
    hide(pollDetailSaveButton);
  }

}

/**
 * Saves the changed inputs (not groups and subgroups)
 * @param pollID int ID from poll
 */
function EditPollSave(pollID) {
  let inputTitle = document.getElementById("inputDetailTitel");
  let inputDate = document.getElementById("inputDetailFinishDate");
  let inputDescription = document.getElementById("inputDetailDescription");

  let oldValueTitle = document.getElementById("inputDetailTitel_old").value;
  let oldValueDate = document.getElementById("inputDetailFinishDate_old").value;
  let oldValueDescription = document.getElementById("inputDetailDescription_old").value;

  let pollDetailSaveButton = document.getElementById("pollDetailSaveButton");

  let success = true;

  if(inputTitle.value !== oldValueTitle) {
    xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) {
      if(xhttp.responseText === "1") {

      } else {
        success = false;
      }
    },'action=editTitleFromPoll&title=' + inputTitle.value + '&pollID=' + pollID);
  }

  if(inputDescription.value !== oldValueDescription) {
    xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) {
      if(xhttp.responseText === "1") {

      } else {
        success = false;
      }
    },'action=editDescriptionFromPoll&description=' + inputDescription.value + '&pollID=' + pollID);
  }

  if(inputDate.value !== oldValueDate) {
    xhr_post('../../../logic/pollHandler/administration.php', function (xhttp) {
      if(xhttp.responseText === "1") {

      } else {
        success = false;
      }
    },'action=editDateFromPoll&date=' + inputDate.value + '&pollID=' + pollID);
  }

  hide(pollDetailSaveButton);
  switchInputsForPollDetails();

  if(success) {
    showModalSuccessAlert("Daten wurden gespeichert!")
  } else {
    showModalErrorAlert("Daten konnten nicht gespeichert werden!");
  }
}