<?php
require_once('../logic/userHandler.php');
require_once('../logic/groupHandler.php');
require_once('../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}

?>

<div id="constantInputBox" class="locked"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
    <!-- Margin bottom because of mobile device support -->
		<ul class="list-group" id="sideMenu" style="margin-bottom: 20px;">

		</ul>
	</div>

  <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
    <?php echo parseStandardAlerts(); ?>

    <div id="main_content">

		</div>
	</div>
	<div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
</div>

<div id="modalContainer"></div>
