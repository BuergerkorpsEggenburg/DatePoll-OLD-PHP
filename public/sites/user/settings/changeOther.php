<?php
require_once('../../../logic/userHandler.php');
$user = getUsers($_SESSION['userID']);
$permissions = getPermissions();
// Darf der Benutzer seine eigenen Notizen sehen?
if ($permissions['isAdmin'])
	$permissions = true;
else
	$permissions = false;
?>
<input id="contentTitle" type="hidden" data-title="edit/profile/other"/>

<div class="card">
	<div class="card-header">
		<i class="fas fa-info-circle"></i> Andere Informationen
		<button id="otherSaveButton" class="btn btn-outline-success float-right" style="margin-right: 10px;"
		        onclick="editOtherDataByHimself()">
			Speichern
		</button>
	</div>
	<div class="card-body">

		<div class="col-12 w-100">
			<div class="form-group text-center">
				<label for="userNotes">Beschreibung</label><br/>
				<textarea maxlength="2000" id="userNotes" class="w-100"
				          placeholder="Hier kannst du dich selbs ein wenig beschreiben, wenn du magst :D"><?php echo $user['userNotes']; ?></textarea>
			</div>
		</div>

		<?php if ($permissions) { ?>
			<div class="col-12 w-100">
				<div class="form-group text-center">
					<label for="adminNotes">Admin-Notizen</label><br/>
					<textarea maxlength="2000" id="adminNotes" class="w-100"
					          placeholder="Hier kannst du wichtige Notizen über den Benutzer eintragen"><?php echo $user['adminNotes']; ?></textarea><br/>
					<small class="small">Diese Notizen können nur Benuter mit Rechten sehen, die nicht dieser Benutzer
						sind (außer Admins ^^)
					</small>
				</div>
			</div>
		<?php } ?>

		<input type="hidden" id="inputUserIDOtherChange" value="<?php echo $_SESSION['userID']; ?>">
	</div>
</div>
