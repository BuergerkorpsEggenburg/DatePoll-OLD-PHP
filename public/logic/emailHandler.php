<?php

//Load composer's autoloader
require_once __DIR__ . '/../../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(__DIR__ . '/cacheHandler.php');
require_once(__DIR__ . '/../keys.php');
require_once(__DIR__ . '/../sites/emailtemplates/resetPasswordTemplate.php');
require_once(__DIR__ . '/../sites/emailtemplates/activationTemplate.php');
require_once(__DIR__ . '/../sites/emailtemplates/changePasswordTemplate.php');
require_once(__DIR__ . '/../sites/emailtemplates/changeEmailTemplate.php');

function sendResetEmailToUser($emailAdress, $name, $code)
{
  $body = getEmailBodyResetPassword($emailAdress, $name, $code);

  sendEmailToUser($emailAdress, 'DatePoll', $name, 'DatePoll - Password Reset Verifizierung', $body, $body, true);
}

function sendActivationCodeToUser($emailAddress, $name, $code)
{
  $body = getEmailBodyActivation($emailAddress, $name, $code);

  sendEmailToUser($emailAddress, 'DatePoll', $name, 'DatePoll - Aktivierungscode', $body, $body, true);
}

function sendPasswordChangeEmailToUser($emailAddress, $name, $code)
{
  $body = getEmailBodyChangePassword($emailAddress, $name, $code);

  sendEmailToUser($emailAddress, 'DatePoll', $name, 'DatePoll - Passwort Änderung', $body, $body, true);
}

function sendEmailChangeEmailToUser($emailAddress, $name, $code) {
  $body = getEmailBodyChangeEmail($emailAddress, $name, $code);

  sendEmailToUser($emailAddress, 'DatePoll', $name, 'DatePoll - Email-Adresse Änderung', $body, $body, true);
}

function sendEmailToUser($emailAddress, $senderName, $name, $subject, $body, $altBody, $asynchron = false)
{
  if($asynchron) {
    $body_file = putIntoCache($body);
    $sender_file = putIntoCache($senderName);
    $name_file = putIntoCache($name);
    $subject_file = putIntoCache($subject);

    execInBackground('php ' . __DIR__ . '/asynchronEmailSending.php ' . getEmailHost() . ' ' . getEmailUsername()
      . ' ' . getEmailPassword() . ' ' . getEmailEncryption() . ' ' . getEmailPort() . ' ' . $sender_file . ' '
      . $emailAddress . ' ' . $name_file . ' ' . $body_file . ' '. $body_file . ' ' . $subject_file);

    return;
  }

  $mail = new PHPMailer(false); // Passing `true` enables exceptions
  try {
    //Server settings
    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output

    $mail->CharSet = 'UTF-8';
    // Set mailer to use SMTP
    $mail->isSMTP();
    // Specify main and backup SMTP servers
    $mail->Host = getEmailHost();
    // Enable SMTP authentication
    $mail->SMTPAuth = true;
    // SMTP username
    $mail->Username = getEmailUsername();
    // SMTP password
    $mail->Password = getEmailPassword();
    // Enable TLS encryption, `ssl` also accepted
    $mail->SMTPSecure = getEmailEncryption();
    // TCP port to connect to
    $mail->Port = getEmailPort();

    $mail->setFrom(getEmailUsername(), $senderName);

    // Send to
    $mail->addAddress($emailAddress, $name);

    $mail->addReplyTo(getEmailUsername(), 'Contact');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true); // Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AltBody = $altBody;

    $mail->send();
    //echo 'Message has been sent';
  } catch (Exception $e) {
    //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
  }
}

function execInBackground($cmd) {
  if (substr(php_uname(), 0, 7) == "Windows"){
    pclose(popen("start /B ". $cmd, "r"));
  }
  else {
    exec($cmd . " > /dev/null &");
  }
}
