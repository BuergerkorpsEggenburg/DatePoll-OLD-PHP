<?php
require_once ('../../logic/groupHandler.php');
require_once ('../../logic/userHandler.php');
require_once ('../../logic/parser.php');

if (!canAccessBasicPage() && (isAdmin() || canManageUsers() || canManageGroups() || canManageSubgroups())){
	echo 'false';
	die();
}

if (!(isset($_REQUEST['userID']) && isset($_REQUEST['currentSubgroupID']))){
	echo 'false';
	die();
}

$groups = getGroups();
$temp = array();
foreach ($groups as $group) {
	$subgroups = '';

	$conn = connect();
	$stmt = $conn->prepare("SELECT subgroupID, name FROM subgroups WHERE groupID = :groupID");
	$stmt->bindParam(':groupID', $group['groupID'], PDO::PARAM_INT);
	$stmt->execute();
	$subgroups = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$tmp = '';
	foreach ($subgroups as $subgroup) {
		$button = parseTag('button', '<i class="fas fa-check"></i>','class="btn btn-success float-right" onclick="moveUserSubmit(' . $_REQUEST['userID'] . ', ' . $_REQUEST['currentSubgroupID'] . ', ' . $subgroup['subgroupID'].')"');
		$tmp .= parseCard(parseCardHeader($subgroup['name'] . $button), '', '');
	}

	array_push($temp, ['id' => $group['groupID'].'subAccordion',
			'header' => $group['name'],
			'body' => $tmp]);
}
$user = getUsers($_REQUEST['userID']);
echo parseModal( 'Verschiebe ' . $user['firstname'] . ' ' . $user['surname'], parseAccordion($temp), 'moveUser');