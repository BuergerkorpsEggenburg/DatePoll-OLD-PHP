<?php

require_once(__DIR__ . '/../dbConnector.php');
require_once(__DIR__ . '/../emailHandler.php');
require_once(__DIR__ . '/../userHandler.php');
require_once(__DIR__ . '/../validationHandler.php');
require_once (__DIR__.'/../cacheHandler.php');

require_once __DIR__ . '/../../../vendor/autoload.php';

use Respect\Validation\Validator as v;

if (isset($_REQUEST['action'])) {
  switch ($_REQUEST['action']) {
    case 'editUserByHimself':
      $res = editUserByHimself($_REQUEST);
      break;
    case 'sendPasswordChangeEmail':
      $res = sendVerificationEmailForPasswordChange();
      break;
    case 'changePasswordByHimself':
      $oldPassword = $_REQUEST['oldPassword'];
      $newPassword = $_REQUEST['newPassword'];
      $newPasswordAgain = $_REQUEST['newPasswordAgain'];
      $verificationCode = $_REQUEST['verificationCode'];

      $res = changePasswordByHimself($oldPassword, $newPassword, $newPasswordAgain, $verificationCode);
      break;
    case 'editNotes':
      $res = editNotes($_REQUEST);
      break;
    case 'checkPassword':
      $password = $_REQUEST['password'];
      $res = checkPasswordForUserID($password);
      break;
    case 'checkCode':
      $code = $_REQUEST['code'];
      $res = checkCode($code);
      break;
    case 'sendEmailChangeEmail':
      $res = sendVerificationEmailForEmailChange();
      break;
    case 'changeEmailByHimself':
      $password = $_REQUEST['password'];
      $email = $_REQUEST['email'];
      $code = $_REQUEST['code'];
      $res = changeEmailByHimself($password, $email, $code);
      break;
    case 'telephoneNumberAlreadyExsits':
      $number = $_REQUEST['telephoneNumber'];
      $res = telephoneNumberAlreadyExsitsForUserID($number);
      break;
    case 'addTelephoneNumber':
      $number = $_REQUEST['telephoneNumber'];
      $label = $_REQUEST['label'];
      $res = addTelephoneNumber($number, $label);
      break;
    case 'removeTelephoneNumber':
      $number = $_REQUEST['telephoneNumber'];
      $res = removeTelephoneNumber($number);
      break;
  }

  echo $res;
}

function editUserByHimself($userdata)
{
  if (isset($userdata) AND !empty($userdata)) {

    if (!(v::stringType()->noWhitespace()->notEmpty()->length(2, 128)->validate($userdata['firstname']))) {
      return 'valid_firstname';
    }

    if (!(v::stringType()->noWhitespace()->notEmpty()->length(2, 128)->validate($userdata['surname']))) {
      return 'valid_surname';
    }

    if (!(v::date()->validate($userdata['birthdate']))) {
      return 'valid_birthdate';
    }

    $title = "";
    $streetname = "";
    $streetnumber = "";
    $location = "";
    $zip_code = "";

    if (isset($userdata['title']) AND !empty($userdata['title'])) {
      $title = $userdata['valid_title'];

      if (!(v::stringType()->notEmpty()->length(1, 64)->validate($title))) {
        return 'valid_title';
      }
    }

    if (isset($userdata['streetname']) AND !empty($userdata['streetname'])) {
      $streetname = $userdata['streetname'];

      if (!(v::stringType()->notEmpty()->length(1, 128)->validate($streetname))) {
        return 'valid_streetname';
      }
    }

    if (isset($userdata['streetnumber']) AND !empty($userdata['streetnumber'])) {
      $streetnumber = $userdata['streetnumber'];

      if (!(v::stringType()->noWhitespace()->notEmpty()->length(1, 16)->validate($streetnumber))) {
        return 'valid_streetnumber';
      }
    }

    if (isset($userdata['location']) AND !empty($userdata['location'])) {
      $location = $userdata['location'];

      if (!(v::stringType()->notEmpty()->length(1, 128)->validate($location))) {
        return 'valid_location';
      }
    }

    if (isset($userdata['zip_code']) AND !empty($userdata['zip_code'])) {
      $zip_code = $userdata['zip_code'];

      if (!(v::intVal()->between(1000, 9999)->validate($userdata['zip_code']))) {
        return 'valid_zipcode';
      }
    }

    try {
      $conn = connect();

      $stmt = $conn->prepare('UPDATE users SET firstname= :firstname, surname= :surname, title= :title, 
                                            location= :location, streetname= :streetname, streetnumber= :streetnumber,
                                            zip_code= :zip_code, birthdate= :birthdate WHERE userID = :userID');

      $stmt->bindParam(':firstname', $userdata['firstname'], PDO::PARAM_STR, 128);
      $stmt->bindParam(':surname', $userdata['surname'], PDO::PARAM_STR, 128);
      $stmt->bindParam(':title', $title, PDO::PARAM_STR, 64);
      $stmt->bindParam(':location', $location, PDO::PARAM_STR, 128);
      $stmt->bindParam(':streetname', $streetname, PDO::PARAM_STR, 128);
      $stmt->bindParam(':streetnumber', $streetnumber, PDO::PARAM_STR, 16);
      $stmt->bindParam(':zip_code', $zip_code, PDO::PARAM_INT, 11);
      $stmt->bindParam(':birthdate', $userdata['birthdate'], PDO::PARAM_STR);
      $stmt->bindParam(':userID', $_SESSION['userID'], PDO::PARAM_INT);

      $stmt->execute();

      return 'successful';
    } catch (PDOException $e) {
      return 'unknownError';
    }

  } else {
    return 'dataNotSet';
  }
}

function sendVerificationEmailForPasswordChange()
{
  $email = getEmailByUserID($_SESSION['userID']);

  $code = rand(100000, 999999);

  activationCode($_SESSION['userID'], $code);

  sendPasswordChangeEmailToUser($email, getFirstnameAndLastnameByUserID($_SESSION['userID']), $code);

  return "successful";
}

function changePasswordByHimself($oldPassword, $newPassword, $newPasswordAgain , $verificationCode) {
  $userID = $_SESSION['userID'];

  $conn = connect();

  $stmt = $conn->prepare('SELECT password FROM users WHERE userID = :userID');
  $stmt->bindParam(':userID', $userID);
  $stmt->execute();
  $hashed_oldPassword_in_database = $stmt->fetch()[0];

  $hashed_oldPassword = hash("sha512", $oldPassword . $userID);

  if($hashed_oldPassword_in_database != $hashed_oldPassword) {
    return 'oldPasswordIncorrect';
  }

  if(!isValidPassword($newPassword)) {
    return 'newPasswordInvalid';
  }

  if ($newPassword != $newPasswordAgain) {
    return 'newPasswordsNotEqual';
  }

  if($verificationCode != activationCode($userID, -11)) {
    return 'verificationCodeIncorrect';
  }

  $hashed_newPassword = hash('sha512', $newPassword . $userID);

  $stmt = $conn->prepare('UPDATE users SET password = :password WHERE userID = :userID');
  $stmt->bindParam(':userID', $userID);
  $stmt->bindParam(':password', $hashed_newPassword);
  $stmt->execute();

  activationCode($userID, 0);

  return 'successful';
}

function checkPasswordForUserID($password) {
  $hashed_password = hash('sha512', $password . $_SESSION['userID']);

  $conn = connect();

  $stmt = $conn->prepare('SELECT password FROM users WHERE userID = :userID AND password = :password;');
  $stmt->bindParam(':userID', $_SESSION['userID']);
  $stmt->bindParam(':password', $hashed_password);

  $stmt->execute();

  if (count($stmt->fetchAll()) > 0)
    return true;
  else
    return false;
}

function checkCode($code) {
  $conn = connect();

  $stmt = $conn->prepare('SELECT activationCode FROM users WHERE userID = :userID AND activationCode = :activationCode;');
  $stmt->bindParam(':userID', $_SESSION['userID']);
  $stmt->bindParam(':activationCode', $code);

  $stmt->execute();

  if (count($stmt->fetchAll()) > 0)
    return "true";
  else
    return "false";
}

function sendVerificationEmailForEmailChange()
{
  $email = getEmailByUserID($_SESSION['userID']);

  $code = rand(100000, 999999);

  $conn = connect();

  $stmt = $conn->prepare('UPDATE users SET activationCode = :code where userID = :userID;');
  $stmt->bindParam(':userID', $_SESSION['userID']);
  $stmt->bindParam(':code', $code);
  $stmt->execute();

  sendEmailChangeEmailToUser($email, getFirstnameAndLastnameByUserID($_SESSION['userID']), $code);

  return "successful";
}

function changeEmailByHimself($password, $email, $code) {
  if(!checkPasswordForUserID($password)) {
    return 'passwordIncorrect';
  }

  if(!isEmail($email)) {
    return 'invalidEmail';
  }

  $userID = $_SESSION['userID'];

  if($code != activationCode($userID, -11)) {
    return 'verificationCodeIncorrect';
  }
  activationCode($userID, 0);

  emailAddress($userID, $email);
  return 'successful';
}

function telephoneNumberAlreadyExsitsForUserID($telephoneNumber) {
  $telephoneNumber = str_replace('+', '', $telephoneNumber);
  $telephoneNumber = str_replace(' ', '', $telephoneNumber);

  $conn = connect();

  $stmt = $conn->prepare('SELECT number FROM telephone_numbers WHERE userID = :userID AND number = :number;');
  $stmt->bindParam(':userID', $_SESSION['userID']);
  $stmt->bindParam(':number', $telephoneNumber);

  $stmt->execute();

  if (count($stmt->fetchAll()) > 0)
    return true;
  else
    return false;
}

function addTelephoneNumber($telephoneNumber, $label) {
  $telephoneNumber = str_replace('+', '', $telephoneNumber);
  $telephoneNumber = str_replace(' ', '', $telephoneNumber);

  if(!isTelephoneNumber($telephoneNumber)) {
    return 'telephoneNumberIsNotANumber';
  }

  if(!isValidLabel($label)) {
    return 'labelIsNotValid';
  }

  if(telephoneNumberAlreadyExsitsForUserID($telephoneNumber)) {
    return 'telephoneNumberAlreadyExsits';
  }

  $conn = connect();

  $stmt = $conn->prepare("INSERT INTO telephone_numbers(userID, number, label) VALUES (:userID,:telephoneNumber,:label)");

  $stmt->bindParam(':userID', $_SESSION['userID']);
  $stmt->bindParam(':telephoneNumber', $telephoneNumber);
  $stmt->bindParam(':label', $label);

  $stmt->execute();

  return "successful";
}

function removeTelephoneNumber($telephoneNumber) {
  if(!telephoneNumberAlreadyExsitsForUserID($telephoneNumber)) {
    return 'telephoneNumberDoesNotExist';
  }

  $conn = connect();

  $stmt = $conn->prepare("DELETE FROM `telephone_numbers` WHERE userID = :userID AND `number` = :telephoneNumber");

  $stmt->bindParam(':userID', $_SESSION['userID']);
  $stmt->bindParam(':telephoneNumber', $telephoneNumber);

  $stmt->execute();

  return 'successful';
}