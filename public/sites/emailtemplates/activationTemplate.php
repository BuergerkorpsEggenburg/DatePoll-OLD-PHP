<?php

require_once (__DIR__ . '/bootstrapTemplate.php');

/**
 * Returns the body for the email to be sent to the user
 *
 * @param $name			name of the user like 'Joe Average'
 * @param $code			activation code
 * @param $link     link to the homepage, if null: uses parameter specified in keys.php with getServerAddress()
 * @return string		body of the email
 */
function getEmailBodyActivation($emailAddress, $name , $code){
  $link = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://'. $_SERVER['HTTP_HOST'].'/startpage.php?emailAddress='
    .$emailAddress.'&authenticationCode='.$code .'&do=signup';
  $websitelink = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://'. $_SERVER['HTTP_HOST'].'/';

  return '
<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">

	'.getBootstrapCSS().'
	<style>
    .footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 60px;
    }
  </style>
</head>
<body>
<nav class="navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="'.$websitelink.'">DatePoll</a>
  <div class="navbar-nav">
    <div class="nav-item nav-link active">Account verifizierung</div>
  </div>
</nav>

<div class="card" style="margin-top: 20px; margin-left: 40px; margin-right: 40px;">
  <div class="card-body">
    <h5 class="card-title">Hallo '.$name.'</h5>
    <h6 class="card-subtitle mb-2 text-muted">Du willst einen DatePoll Account mit dieser Email-Adressse erstellen.</h6>
    <p class="card-text">
      Du hast zwei Möglichkeiten ab hier fortzufahren. Entweder kopierst du diesen Code
      <code>'.$code.'</code>
      und gibst ihn auf der Website ein oder du klickst einfach auf folgenden Link.
    </p>
    <a href="'.$link.'" class="card-link">Hier klicken um deinen Account zu aktivieren.</a>
  </div>
  <div class="card-footer text-muted">
    Du hast keinen DatePoll Account erstellt? Ignoriere diese Email.
  </div>
</div>

<nav class="navbar navbar-dark bg-primary footer">
  <div class="navbar-nav">
    <a class="nav-item nav-link">Impressum</a>
  </div>
</nav>

</body>
</html>
';

}
?>