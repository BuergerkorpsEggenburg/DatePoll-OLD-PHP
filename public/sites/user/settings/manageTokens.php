<?php
require_once(__DIR__ . '/../../../logic/userHandler.php');
require_once(__DIR__ . '/../../../logic/userHandler/tokenHandler.php');
require_once(__DIR__ . '/../../../logic/parser.php');

if (!canAccessBasicPage()) {
  echo false;
  die();
}
?>

<div class="card">
  <div class="card-header">
    <i class="fas fa-address-card"></i> Angemeldete Geräte verwalten
  </div>
  <div class="card-body">
    <div class="row">
      <div class="table-responsive">
        <?php
        $table = parseTableHead(parseTableData('Erstellungsdatum')
          . parseTableData('Letzter Logindatum')
          . parseTableData('Browser Name')
          . parseTableData('Browser Version')
          . parseTableData('User Agent')
          . parseTableData('Betriebssystem')
          . parseTableData('Beenden'));
        $tokens = getAllTokens($_SESSION['userID']);
        foreach ($tokens as $token) {
          $row = "";

          $row .= parseTableData($token['createdDate']);
          $row .= parseTableData($token['lastUsedTokenDate']);
          $row .= parseTableData($token['browserName']);
          $row .= parseTableData($token['browserVersion']);
          $row .= parseTableData($token['userAgent']);
          $row .= parseTableData($token['operatingSystem']);
          $row .= parseTableData('
            <button class="btn btn-danger" data-toggle="tooltip" data-placement="right"
                title="Beende diese Session sodass sich dieses Geräte nicht mehr anmelden kann" 
                onclick="removeToken(\''.$token['token'].'\')">
              <i class="fas fa-trash"></i>
            </button>');

          $row = parseTableRow($row);
          $table .= $row;
        }
        $table = parseTable($table, 'class="table table-bordered table-hover"');

        echo $table;
        ?>
      </div>
    </div>
  </div>
</div>