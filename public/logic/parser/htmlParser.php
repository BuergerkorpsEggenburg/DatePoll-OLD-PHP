<?php

/**
 * parses any HTML-tag with the given arguments
 * @param string $tagname	 				the name of the tag
 * @param string $content					the content of the tag
 * @param string|null $attributes	optional attributes of the tag
 * @return string									parsed tag
 */
function parseTag($tagname, $content, $attributes = null) {
	return "<$tagname $attributes>\n\t$content\n</$tagname>";
}

/**
 * parses a HTML-div-container with the given arguments
 * @param string $content						the content of the container
 * @param string $class							optional classes of the container
 * @param string|null $attributes		optional attributes of the container
 * @return string										parsed container
 */
function parseDiv($content, $class = null, $attributes = null) {
	return parseTag("div", $content, "class='$class' ".$attributes);
}

/**
 * parses a HTML-table with the given arguments
 * @param string $content						the content of the table
 * @param string|null $attributes		optional attributes of the table
 * @return string										parsed table
 */
function parseTable($content, $attributes = null) {
	return parseTag("table", $content, $attributes);
}

/**
 * parses an HTML-table-head with the given arguments
 * @param string $content						the content of the table-heaad
 * @param string|null $attributes		optional attributes of the table-head
 * @return string										parsed table-head
 */
function parseTableHead($content, $attributes = null) {
	return parseTag("thead", $content, $attributes);
}

/**
 * parses an HTML-table-body with the given arguments
 * @param string $content						the content of the table-body
 * @param string|null $attributes		optional attributes of the table-body
 * @return string										parsed table-body
 */
function parseTableBody($content, $attributes = null) {
	return parseTag("tbody", $content, $attributes);
}

/**
 * parses a HTML-table-footer with the given arguments
 * @param string $content						the content of the table-footer
 * @param string|null $attributes		optional attributes of the table-footer
 * @return string										parsed table-footer
 */
function parseTableFooter($content, $attributes = null) {
	return parseTag("tfoot", $content, $attributes);
}

/**
 * parses a HTML-table-row with the given arguments
 * @param string $content						the content of the table-row
 * @param string|null $attributes		optional attributes of the table-row
 * @return string										parsed table-row
 */
function parseTableRow($content, $attributes = null) {
	return parseTag("tr", $content, $attributes);
}

/**
 * parses HTML-table-data with the given arguments
 * @param string $content						the content of the table-data
 * @param string|null $attributes		optional attributes of the table-data
 * @return string										parsed table-data
 */
function parseTableData($content, $attributes = null) {
	return parseTag("td", $content, $attributes);
}
?>