<?php
if(!isset($_REQUEST['permission'])){
	echo false;
	die();
}
require_once ('../../logic/userHandler.php');
require_once ('../../logic/parser.php');

$users = getUsersNotHavingPermission($_REQUEST['permission']);
if(count($users) == 0)
	$_REQUEST['noMoreUsers'] = 1

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="ModalTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal"><?php echo $_REQUEST['permission']?>-Recht vergeben</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if (isset($_REQUEST['error']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-danger alert-dismissible fade show" role="alert">
	                <strong>Recht vergeben fehlgeschlagen!</strong> Bitte überprüfe deine Eingaben nochmals!
	              </div>
	            </li>';
				if (isset($_REQUEST['noMoreUsers']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-warning alert-dismissible fade show" role="alert">
	                <strong>Es gibt keine weiteren Benutzer, denen du Rechte dafür geben könntest!</strong>
	              </div>
	            </li>';

				$table = parseTableHead(parseTableData('userID') . parseTableData('Vorname') . parseTableData('Nachname'));
				foreach ($users as $user){
					$onclick = "onclick='setPermission(\"".$_REQUEST['permission']."\", ".$user['userID'].", \"true\")' data-dismiss='modal'";
					$row = "";

					$row .= parseTableData($user['userID'], $onclick);
					$row .= parseTableData($user['firstname'], $onclick);
					$row .= parseTableData($user['surname'], $onclick);

					$row = parseTableRow($row);
					$table .= $row;
				}
				$table = parseTable($table, 'class="table table-bordered table-hover"');
				echo $table;

				?>
			</div>
		</div>
	</div>
</div>