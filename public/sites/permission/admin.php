<?php
require_once('../../logic/userHandler.php');
require_once('../../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}
if (!getPermissions()['isAdmin'] == 1) {
	return false;
	die();
}
?>
<input id="contentTitle" type="hidden" data-title="edit/permissions"/>

<h1 class="card-title" style="text-align: center;"><i class="fas fa-tasks"></i> Rechte verwalten</h1>
<div class="card-deck">

	<div class="col-sm-12 col-md-6" style="max-height: 100%;">
		<div class="card">
			<div class="card-header text-center">
				Benutzer-Liste
			</div>
			<div class="card-body text-center">
				<?php
				$table = parseTableHead(parseTableData('userID') . parseTableData('Vorname') . parseTableData('Nachname'));
				$users = getUsers();
				foreach ($users as $user) {
					$detail = 'onclick="getUserPermissions(' . $user['userID'] . ')"';
					$row = "";

					$row .= parseTableData($user['userID'], $detail);
					$row .= parseTableData($user['firstname'], $detail);
					$row .= parseTableData($user['surname'], $detail);

					$row = parseTableRow($row);
					$table .= $row;
				}
				$table = parseTable($table, 'class="table table-bordered table-hover"');

				echo $table;
				?>
			</div>
		</div>
	</div>

	<div class="col-sm-12 col-md-6" style="max-height: 100%; display: none;">
		<div class="card">
			<div class="card-header text-center">
				Rechte
				<button class="btn btn-outline-warning float-right" onclick="hideDetails('permissionDetail')">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="card-body text-center" id="permissionDetail"></div>
		</div>
	</div>

</div>