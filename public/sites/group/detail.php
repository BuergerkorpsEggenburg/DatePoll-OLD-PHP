<?php
require_once('../../logic/userHandler.php');
require_once('../../logic/groupHandler.php');
require_once('../../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}
if (!getPermissions()['canManageGroups'] == 1 and !getPermissions()['isAdmin'] == 1) {
	return false;
	die();
}

$table = '';
$group = getGroups((int)$_REQUEST['groupID']);

$table .= parseTableRow(parseTableData('groupID') . parseTableData($group['groupID']));
$table .= parseTableRow(parseTableData('Titel') . parseTableData($group['name']));
$table .= parseTableRow(parseTableData('Beschreibung') . parseTableData($group['description']));
$table .= parseTableRow(parseTableData(parseTag('button', 'Gruppen verwalten', 'class="btn btn-primary" onclick="manageSubgroup('.$group['groupID'].')"'), 'colspan="2"'));

$table = parseTable($table, 'class="table table-bordered"');

echo $table;
?>