<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/userHandler.php';

use Respect\Validation\Validator as v;

if (isset($_REQUEST['action'])) {
  switch ($_REQUEST['action']) {
    case 'isEmail':
      $email = $_REQUEST['email'];
      $res = isEmail($email);
      break;
    case 'isValidPassword':
      $password = $_REQUEST['password'];
      $res = isValidPassword($password);
      break;
    case 'isTelephoneNumber':
      $number = $_REQUEST['telephonenumber'];
      $res = isTelephoneNumber($number);
      break;
    case 'isValidLabel':
      $label = $_REQUEST['label'];
      $res = isValidLabel($label);
      break;
    case 'isValidPollTitle':
      $title = $_REQUEST['title'];
      $res = isValidPollTitle($title);
      break;
    case 'isValidDate':
      $date = $_REQUEST['date'];
      $res = isValidDate($date);
      break;
    case 'isValidPollDescription':
      $description = $_REQUEST['description'];
      $res = isValidPollDescription($description);
      break;
    case 'isValidUserTitle':
      $res = isValidUserTitle($_REQUEST['title']);
      break;
    case 'isValidFirstname':
      $res = isValidFirstname($_REQUEST['firstname']);
      break;
    case 'isValidSurname':
      $res = isValidSurname($_REQUEST['surname']);
      break;
    case 'isValidStreetname':
      $res = isValidStreetname($_REQUEST['streetname']);
      break;
    case 'isValidStreetnumber':
      $res = isValidStreetnumber($_REQUEST['streetnumber']);
      break;
    case 'isValidZipCode':
      $res = isValidZipCode($_REQUEST['zipcode']);
      break;
    case 'isValidLocation':
      $res = isValidLocation($_REQUEST['location']);
      break;
    case 'emailAddressAlreadyUsed':
      $res = !emailAddressAlreadyUsed($_REQUEST['email']);
      break;
    default:
      $res = "";
      break;
  }

  echo $res;
}

function isEmail($email) {
  return v::email()->validate($email);
}

function isValidPassword($password) {
  return v::stringType()->noWhitespace()->notEmpty()->length(6, 512)->validate($password);
}

function isTelephoneNumber($number) {
  return v::phone()->validate($number);
}

function isValidLabel($label) {
  return v::stringType()->noWhitespace()->length(1, 32)->validate($label);
}

function isValidPollTitle($title) {
  return v::stringType()->length(1, 64)->validate($title);
}

function isValidPollDescription($description) {
  return v::stringType()->length(0, 8192)->validate($description);
}

function isValidDate($date) {
  return v::date()->validate($date);
}

function isValidUserTitle($title) {
  return v::stringType()->length(0, 64)->validate($title);
}

function isValidFirstname($firstname) {
  return v::stringType()->noWhitespace()->notEmpty()->length(2, 128)->validate($firstname);
}

function isValidSurname($surname) {
  return v::stringType()->noWhitespace()->notEmpty()->length(2, 128)->validate($surname);
}

function isValidStreetname($streetname) {
  return v::stringType()->length(0, 128)->validate($streetname);
}

function isValidStreetnumber($streetnumber) {
  return v::stringType()->length(0, 16)->validate($streetnumber);
}

function isValidZipCode($zipcode) {
  return v::intVal()->between(1000, 9999)->validate($zipcode);
}

function isValidLocation($location) {
  return v::stringType()->length(0, 128)->validate($location);
}