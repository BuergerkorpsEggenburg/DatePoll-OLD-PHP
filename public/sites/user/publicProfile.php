<?php
require_once('../../logic/parser.php');
require_once('../../logic/userHandler.php');

$permissions = getPermissions();
$user = getUsers($_REQUEST['userID']);

if ($permissions['isAdmin'] || $_REQUEST['userID'] != $_SESSION['userID']) {
	//Hat der Benutzer eine Berechtigung?
	if ($permissions['isAdmin'] ||
			$permissions['canManageUsers'] ||
			$permissions['canManagePolls'] ||
			$permissions['canManageOrganisation'] ||
			$permissions['canManageGroups'] ||
			$permissions['canManageSubgroups'])
		$permissions = true;
	else
		$permissions = false;
} else
	$permissions = false;

$profile = "<h1 class='text-center'>" . $user['title'] . " " . $user['firstname'] . " " . $user['surname'] . "</h1>\n";
$profile .=
		'<div class="form">
					<div class="form-row">
						<div class="col-6 col-sm-6 col-md-2 col-lg-3 col-xl-3">
							<div class="form-group">
								<label for="inputTitle">Titel</label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputTitle" value="' . $user['title'] . '" disabled>
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-5 col-lg-4 col-xl-4">
							<div class="form-group">
								<label for="inputFirstname">Vorname</label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputFirstname" name="inputFirstname" value="' . $user['firstname'] . '" disabled>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
							<div class="form-group">
								<label for="inputSurname">Nachname</label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputSurname" name="inputSurname" value="' . $user['surname'] . '" disabled>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-row">
						<div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5">
							<div class="form-group">
								<label for="inputBirthdate">Geburtsdatum</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<label for="inputBirthdate" class="input-group-text"><i class="fas fa-birthday-cake"></i></label>
									</div>
									<input type="date" class="form-control" id="inputBirthdate" name="inputBirthdate" value="' . $user['birthdate'] . '" disabled>
								</div>
							</div>
						</div>
						
						<div class="col-12 col-sm-12 col-md-8 col-lg-7 col-xl-7">
							<div class="form-group">
								<label for="inputEmail">Email Adresse</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<label for="inputEmail" class="input-group-text">@</label>
									</div>
									<input type="email" class="form-control" id="inputEmail" name="inputEmail" aria-describedby="emailHelp"  value="' . $user['email'] . '" disabled>
								</div>
							</div>
						</div>
						
						<hr />
						
						<div class="col-12 w-100">
							<div class="form-group text-center">
								<label for="userNotes">Beschreibung</label><br/>
								<textarea maxlength="2000" id="userNotes" class="w-100" disabled>'.$user['userNotes'].'</textarea>
							</div>
						</div>
						'.($permissions ? '
						<div class="col-12 w-100">
							<div class="form-group text-center">
								<label for="adminNotes">Admin-Notizen</label><br/>
								<textarea maxlength="2000" id="adminNotes" class="w-100" disabled>'.$user['adminNotes'].'</textarea><br/>
								<small class="small">Diese Notizen können nur Benuter mit Rechten sehen, die nicht dieser Benutzer
									sind (außer Admins ^^)
								</small>
							</div>' : '').'
						</div>
						
					</div>';


echo parseModal('Profil',
		$profile,
		'showProfileOf' . $user['userID'],
		'lg');