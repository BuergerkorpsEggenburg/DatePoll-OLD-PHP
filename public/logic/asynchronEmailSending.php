<?php

//Load composer's autoloader
require_once __DIR__ . '/../../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(__DIR__ . '/cacheHandler.php');


$emailHost = $argv[1];
$emailUsername = $argv[2];
$emailPassword = $argv[3];
$emailEncryption = $argv[4];
$emailPort = $argv[5];

$senderNameFile = $argv[6];
$emailAddress = $argv[7];
$nameFile = $argv[8];
$bodyFile = $argv[9];
$altBodyFile = $argv[10];
$subjectFile = $argv[11];

$name = getFromCacheAndDelete($nameFile);
$senderName = getFromCacheAndDelete($senderNameFile);
$body = getFromCache($bodyFile);
$altBody = getFromCacheAndDelete($altBodyFile);
$subject = getFromCacheAndDelete($subjectFile);

$mail = new PHPMailer(false); // Passing `true` enables exceptions
try {
  //Server settings
  //$mail->SMTPDebug = 2;                                 // Enable verbose debug output

  $mail->CharSet = 'UTF-8';
  // Set mailer to use SMTP
  $mail->isSMTP();
  // Specify main and backup SMTP servers
  $mail->Host = $emailHost;
  // Enable SMTP authentication
  $mail->SMTPAuth = true;
  // SMTP username
  $mail->Username = $emailUsername;
  // SMTP password
  $mail->Password = $emailPassword;
  // Enable TLS encryption, `ssl` also accepted
  $mail->SMTPSecure = $emailEncryption;
  // TCP port to connect to
  $mail->Port = $emailPort;

  $mail->setFrom($emailUsername, $senderName);

  // Send to
  $mail->addAddress($emailAddress, $name);

  $mail->addReplyTo($emailUsername, 'Contact');

  //Attachments
  //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
  //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

  //Content
  $mail->isHTML(true); // Set email format to HTML
  $mail->Subject = $subject;
  $mail->Body = $body;
  $mail->AltBody = $body;

  $mail->send();

  //echo 'Message has been sent';
} catch (Exception $e) {
  //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}