/**
 * checks the stayLoggedIn cookies and performes a login if the token is correct
 */
function stayLoggedIn() {
  let token = getCookie("token");
  let emailAddress = getCookie("emailAddress");

  if(token !== '' && emailAddress !== '') {
    xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {

      switch (xhttp.responseText) {
        case "true":
          window.location.href = "index.php";

          break;
        case "false":
          showModalErrorAlert("Dein Gerät wurde von DatePoll abgemeldet. Melde dich erneut an um wieder angemeldet zu sein!");
          console.log("stayLoggedIn: incorrect");
          break;
        case "notActivated":
          console.log("stayLoggedIn: notActivated");
          break;
        default:
          console.log("stayLoggedIn: Unknown response: " + xhttp.responseText);
          break;
      }


    }, 'action=loginWithToken&emailAddress=' + emailAddress + '&token=' + token);
  }
}

/**
 * deletes all cookies from the browser
 */
function logout() {
  let token = getCookie("token");
  let emailAddress = getCookie("emailAddress");

  if(token !== '' && emailAddress !== '') {
    xhr_post('../../../logic/userHandler/tokenHandler.php', function (xhttp) {
      showSuccessAlert("Du hast dich erfolgreich abgemeldet! Alle Daten wurden gelöscht!");
    }, 'action=deleteToken&token=' + token);

    deleteCookie("token");
    deleteCookie("emailAddress");
  } else {
    showSuccessAlert("Du hast dich erfolgreich abgemeldet!");
  }
}

/**
 * performes a login attempt on success redirect to index.php, on stay logged in create a new token and save it
 */
function login() {
  let emailAddress = document.getElementById("inputEmailAddress");
  let password = document.getElementById("inputPassword");
  let stayLoggedIn = document.getElementById("stayLoggedIn");

  xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {

    switch (xhttp.responseText) {
      case "true":
        if(stayLoggedIn.checked) {
          let nAgt = navigator.userAgent;
          let browserName  = navigator.appName;
          let fullVersion  = ''+parseFloat(navigator.appVersion);
          let nameOffset,verOffset,ix;

          // In Opera, the true version is after "Opera" or after "Version"
          if ((verOffset=nAgt.indexOf("Opera"))!==-1) {
            browserName = "Opera";
            fullVersion = nAgt.substring(verOffset+6);
            if ((verOffset=nAgt.indexOf("Version"))!==-1)
              fullVersion = nAgt.substring(verOffset+8);
          }
          // In MSIE, the true version is after "MSIE" in userAgent
          else if ((verOffset=nAgt.indexOf("MSIE"))!==-1) {
            browserName = "Microsoft Internet Explorer";
            fullVersion = nAgt.substring(verOffset+5);
          }
          // In Chrome, the true version is after "Chrome"
          else if ((verOffset=nAgt.indexOf("Chrome"))!==-1) {
            browserName = "Chrome";
            fullVersion = nAgt.substring(verOffset+7);
          }
          // In Safari, the true version is after "Safari" or after "Version"
          else if ((verOffset=nAgt.indexOf("Safari"))!==-1) {
            browserName = "Safari";
            fullVersion = nAgt.substring(verOffset+7);
            if ((verOffset=nAgt.indexOf("Version"))!==-1)
              fullVersion = nAgt.substring(verOffset+8);
          }
          // In Firefox, the true version is after "Firefox"
          else if ((verOffset=nAgt.indexOf("Firefox"))!==-1) {
            browserName = "Firefox";
            fullVersion = nAgt.substring(verOffset+8);
          }
          // In most other browsers, "name/version" is at the end of userAgent
          else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) <
            (verOffset=nAgt.lastIndexOf('/')) )
          {
            browserName = nAgt.substring(nameOffset,verOffset);
            fullVersion = nAgt.substring(verOffset+1);
            if (browserName.toLowerCase()===browserName.toUpperCase()) {
              browserName = navigator.appName;
            }
          }
          // trim the fullVersion string at semicolon/space if present
          if ((ix=fullVersion.indexOf(";"))!==-1)
            fullVersion=fullVersion.substring(0,ix);
          if ((ix=fullVersion.indexOf(" "))!==-1)
            fullVersion=fullVersion.substring(0,ix);

          // This script sets OSName variable as follows:
          // "Windows"    for all versions of Windows
          // "MacOS"      for all versions of Macintosh OS
          // "Linux"      for all versions of Linux
          // "UNIX"       for all other UNIX flavors
          // "Unknown OS" indicates failure to detect the OS

          let OSName=  "Unknown OS";
          if (navigator.appVersion.indexOf("Win")!==-1) OSName="Windows";
          if (navigator.appVersion.indexOf("Mac")!==-1) OSName="MacOS";
          if (navigator.appVersion.indexOf("X11")!==-1) OSName="UNIX";
          if (navigator.appVersion.indexOf("Linux")!==-1) OSName="Linux";

          xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {

            switch (xhttp.responseText) {
              case "emailAddressDoesNotExist":
                console.log("emailAddressDoesNotExist");

                break;
              case "incorrectPassword":
                console.log("emailAddressDoesNotExist");

                break;
              default:
                let token = xhttp.responseText;
                let expDate = 30*12*10;

                setCookie("token", token, expDate);
                setCookie("emailAddress", emailAddress.value, expDate);

                window.location.href = "index.php";
                break;
            }

          }, 'action=stayLoggedIn&emailAddress=' + emailAddress.value
            + '&browserName=' + browserName
            + '&browserVersion=' + fullVersion
            + '&userAgent=' + navigator.userAgent
            + '&operatingsystem=' + OSName
            + '&password=' + password.value);

        } else {
          window.location.href = "index.php";
        }

        break;
      case "false":
        let groups = document.getElementsByClassName("input-group");
        for (let i = 0; i < groups.length; i++) {
          shake(groups[i]);
        }

        let loginWrongAlert = document.getElementById("loginWrongAlert");
        unhide(loginWrongAlert);
        setTimeout(function () {
          hide(loginWrongAlert);
        }, 5000);

        break;
      case "notActivated":
        showModalErrorAlert("Dein Account wurde leider noch nicht aktiviert! Bitte warte noch auf die Freischaltung.");

        break;
      default:
        console.log("Login: Unkown response: " + xhttp.responseText);
        break;
    }

  }, 'action=login&emailAddress=' + emailAddress.value + '&password=' + password.value);
}

/**
 * sends a email with the verification code
 */
function sendPasswordResetVerification() {
  let emailAddress = document.getElementById("inputEmailAddress");

  xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {

    if (xhttp.responseText === "1") {

      $('#enterEmail').collapse('hide');
      hide(document.getElementById("sendPasswordResetVerificationButton"));

      $('#enterCode').collapse('show');
      unhide(document.getElementById("checkPasswordResetVerficationCodeButton"));

    } else {
      let groups = document.getElementsByClassName("input-group");
      for (let i = 0; i < groups.length; i++) {
        shake(groups[i]);
      }

      showModalErrorAlert("Es exestiert kein Konto für diese Email-Adresse.");
    }

  }, 'action=sendResetEmail&emailAddress=' + emailAddress.value);
}

/**
 * checks the verification code on button click
 */
function checkPasswordResetVerficationCode() {
  let emailAddress = document.getElementById("inputEmailAddress");
  let code = document.getElementById("inputCode");

  xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {

    if (xhttp.responseText === "1") {

      $('#enterCode').collapse('hide');
      hide(document.getElementById("checkPasswordResetVerficationCodeButton"));

      $('#enterNewPasswords').collapse('show');
      unhide(document.getElementById("resetPasswordButton"));

    } else {
      let groups = document.getElementsByClassName("input-group");
      for (let i = 0; i < groups.length; i++) {
        shake(groups[i]);
      }

      showModalErrorAlert("Der Code ist inkorrekt!");
    }

  }, 'action=checkPasswordResetVerificationCode&emailAddress=' + emailAddress.value + '&code=' + code.value);
}

/**
 * resets the password
 */
function resetPassword() {
  let emailAddress = document.getElementById("inputEmailAddress");
  let code = document.getElementById("inputCode");
  let password = document.getElementById("inputPassword");
  let passwordAgain = document.getElementById("inputPasswordAgain");

  xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {

    switch (xhttp.responseText) {
      case "notValidPassword":
        showModalErrorAlert("Das Passwort muss aus mindestens <strong>6</strong> und maximal <strong>512</strong> Zeichen bestehen. Außerdem dürfen keine Leerzeichen vorkommen!");

        break;
      case "passwordAreNotEqual":
        showModalErrorAlert("Die Passwörter sind nicht gleich!");
        resetInput(password);
        resetInput(passwordAgain);

        break;
      case "emailAddressNotUsed":
        showModalErrorAlert("Email!");

        break;
      case "activationCodeIncorrect":
        showModalErrorAlert("Activationcode");

        break;
      case "1":
        $('#enterNewPasswords').collapse('hide');

        hide(document.getElementById("cancelButton"));
        hide(document.getElementById("resetPasswordButton"));
        unhide(document.getElementById("closeModalAfterSuccessButton"));
        unhide(document.getElementById("passwortResetSuccessAlert"));

        break;
    }

  }, 'action=resetPassword&emailAddress=' + emailAddress.value + '&code=' + code.value + '&password=' + password.value + '&passwordAgain=' + passwordAgain.value);

}


/**
 * checks values on input change on signup
 */
function signupValuesChanged() {
  let inputEmailAddress = document.getElementById("inputEmailAddress");
  let inputBirthdate = document.getElementById("inputBirthdate");
  let inputPassword = document.getElementById("inputPassword");
  let inputPassword2 = document.getElementById("inputPassword2");
  let inputTitle = document.getElementById("inputTitle");
  let inputFirstname = document.getElementById("inputFirstname");
  let inputSurname = document.getElementById("inputSurname");
  let inputStreetname = document.getElementById("inputStreetname");
  let inputStreetnumber = document.getElementById("inputStreetnumber");
  let inputZipcode = document.getElementById("inputZipcode");
  let inputLocation = document.getElementById("inputLocation");

  let emailIncorrect = document.getElementById("emailIncorrect");
  let emailAlreadyUsed = document.getElementById("emailAlreadyUsed");
  let birthdateIncorrect = document.getElementById("birthdateIncorrect");
  let passwordIncorrect = document.getElementById("passwordIncorrect");
  let passwordNotEquals = document.getElementById("passwordNotEquals");
  let titleIncorrect = document.getElementById("titleIncorrect");
  let firstnameIncorrect = document.getElementById("firstnameIncorrect");
  let surnameIncorrect = document.getElementById("surnameIncorrect");
  let streetnameIncorrect = document.getElementById("streetnameIncorrect");
  let streetnumberIncorrect = document.getElementById("streetnumberIncorrect");
  let zipcodeIncorrect = document.getElementById("zipcodeIncorrect");
  let locationIncorrect = document.getElementById("locationIncorrect");

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(emailIncorrect);

      xhr_post('../../../logic/validationHandler.php', function (xhttp) {
        if (xhttp.responseText === "1") {
          hide(emailAlreadyUsed);
        } else {
          unhide(emailAlreadyUsed);
        }
      }, 'action=emailAddressAlreadyUsed&email=' + inputEmailAddress.value);

    } else {
      unhide(emailIncorrect);
    }
  }, 'action=isEmail&email=' + inputEmailAddress.value);

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(birthdateIncorrect);
    } else {
      unhide(birthdateIncorrect);
    }
  }, 'action=isValidDate&date=' + inputBirthdate.value);


  if (inputPassword.value !== inputPassword2.value) {
    unhide(passwordNotEquals);
    hide(passwordIncorrect);
  } else {
    hide(passwordNotEquals);

    xhr_post('../../../logic/validationHandler.php', function (xhttp) {
      if (xhttp.responseText === "1") {
        hide(passwordIncorrect);
      } else {
        unhide(passwordIncorrect);
      }
    }, 'action=isValidPassword&password=' + inputPassword.value);
  }

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(titleIncorrect);
    } else {
      unhide(titleIncorrect);
    }
  }, 'action=isValidUserTitle&title=' + inputTitle.value);

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(firstnameIncorrect);
    } else {
      unhide(firstnameIncorrect);
    }
  }, 'action=isValidFirstname&firstname=' + inputFirstname.value);

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(surnameIncorrect);
    } else {
      unhide(surnameIncorrect);
    }
  }, 'action=isValidSurname&surname=' + inputSurname.value);

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(streetnameIncorrect);
    } else {
      unhide(streetnameIncorrect);
    }
  }, 'action=isValidStreetname&streetname=' + inputStreetname.value);

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(streetnumberIncorrect);
    } else {
      unhide(streetnumberIncorrect);
    }
  }, 'action=isValidStreetnumber&streetnumber=' + inputStreetnumber.value);

  if (inputZipcode.value === "") {
    hide(zipcodeIncorrect);
  } else {
    xhr_post('../../../logic/validationHandler.php', function (xhttp) {
      if (xhttp.responseText === "1") {
        hide(zipcodeIncorrect);
      } else {
        unhide(zipcodeIncorrect);
      }
    }, 'action=isValidZipCode&zipcode=' + inputZipcode.value);
  }

  xhr_post('../../../logic/validationHandler.php', function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(locationIncorrect);
    } else {
      unhide(locationIncorrect);
    }
  }, 'action=isValidLocation&location=' + inputLocation.value);
}

/**
 * performes a signup attempt
 */
function signup() {
  let inputEmailAddress = document.getElementById("inputEmailAddress");
  let inputBirthdate = document.getElementById("inputBirthdate");
  let inputPassword = document.getElementById("inputPassword");
  let inputPassword2 = document.getElementById("inputPassword2");
  let inputTitle = document.getElementById("inputTitle");
  let inputFirstname = document.getElementById("inputFirstname");
  let inputSurname = document.getElementById("inputSurname");
  let inputStreetname = document.getElementById("inputStreetname");
  let inputStreetnumber = document.getElementById("inputStreetnumber");
  let inputZipcode = document.getElementById("inputZipcode");
  let inputLocation = document.getElementById("inputLocation");

  xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {

    switch (xhttp.responseText) {
      case "1":
        $('#enterInputs').collapse('hide');
        $('#enterActivationCode').collapse('show');

        hide(document.getElementById("signupButton"));
        unhide(document.getElementById("activationCodeSendButton"));

        showModalSuccessAlert("Die Aktiverungsemail wurde erfolgreich versendet! Bitte überprüfe dein Postfach und gib den Aktivierungscode ein!");

        break;
      case "notValidEmail":
        showModalErrorAlert("Die Email-Adresse ist nicht valide!");

        break;
      case "passwordNotEquals":
        showModalErrorAlert("Die Passwörter sind nicht gleich!");
        resetInput(inputPassword);
        resetInput(inputPassword2);

        break;
      case "notValidPassword":
        showModalErrorAlert("Das Passwort entspricht nicht den Vorgaben!");

        break;
      case "notValidFirstname":
        showModalErrorAlert("Der Vorname entspricht nicht den Vorgaben");

        break;
      case "notValidSurname":
        showModalErrorAlert("Der Nachmame entspricht nicht den Vorgaben!");

        break;
      case "notValidBirthdate":
        showModalErrorAlert("Das Geburtsdatum ist kein Datum!");

        break;
      case "notValidTitle":
        showModalErrorAlert("Der Titel entspricht nicht den Vorgaben!");

        break;
      case "notValidStreetname":
        showModalErrorAlert("Der Straßenname entspricht nicht den Vorgaben!");

        break;
      case "notValidStreetnumber":
        showModalErrorAlert("Die Hausnummer entspricht nicht den Vorgaben!");

        break;
      case "notValidLocation":
        showModalErrorAlert("Der Ortsname entspricht nicht den Vorgaben!");

        break;
      case "notValidZipcode":
        showModalErrorAlert("Die Postleitzahl entspricht nicht den Vorgaben!");

        break;
      case "emailAddressAlreadyUsed":
        showModalErrorAlert("Die Email-Adresse wurde bereits verwendet!");

        break;
       default:
        showModalErrorAlert("Unkown Response: " + xhttp.responseText);

        break;
    }
  }, 'action=signup' +
    '&emailAddress=' + inputEmailAddress.value +
    '&birthdate=' + inputBirthdate.value +
    '&password=' + inputPassword.value +
    '&passwordAgain=' + inputPassword2.value +
    '&title=' + inputTitle.value +
    '&firstname=' + inputFirstname.value +
    '&surname=' + inputSurname.value +
    '&streetname=' + inputStreetname.value +
    '&streetnumber=' + inputStreetnumber.value +
    '&zipcode=' + inputZipcode.value +
    '&location=' + inputLocation.value);
}

/**
 * check verification code on signup
 */
function checkSignupVerificationCode() {
  let inputEmailAddress = document.getElementById("inputEmailAddress");
  let inputCode = document.getElementById("inputCode");

  xhr_post('../../../logic/userHandler/authentication.php', function (xhttp) {
    if(xhttp.responseText === "1") {
      $('#enterActivationCode').collapse('hide');
      $('#verificationSuccess').collapse('show');

      hide(document.getElementById("closeButton"));
      hide(document.getElementById("activationCodeSendButton"));
      unhide(document.getElementById("finishButton"));
    } else {
      showModalErrorAlert("Der Code ist inkorrekt!");
    }
  }, 'action=activateAccount&emailAddress=' + inputEmailAddress.value + '&activationCode=' + inputCode.value);
}