<?php
require_once ('logic/settingsHandler.php');

function getServerAddress() {
  return getSettings()['dbHost'];
}

function getDatabaseName() {
	return getSettings()['dbName'];
}

function getDatabaseUser() {
	return getSettings()['dbUser'];
}

function getDatabasePassword () {
	return getSettings()['dbPassword'];
}

function getEmailHost() {
	return getSettings()['mailHost'];
}

function getEmailUsername() {
	return getSettings()['mailUser'];
}

function getEmailPassword() {
	return getSettings()['mailPassword'];
}

function getEmailPort() {
	return getSettings()['mailPort'];
}

//SSL or TLS
function getEmailEncryption() {
	return getSettings()['mailEncryption'];
}

function getContactEmailAddress() {
	return getSettings()['mailContactAddress'];
}

function getDefaultPassword() {
	return getSettings()['userDefaultPassword'];
}
