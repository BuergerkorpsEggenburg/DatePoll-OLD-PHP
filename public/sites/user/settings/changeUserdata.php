<?php
require_once('../../../logic/userHandler.php');
require_once('../../../logic/parser.php');

$user = getUsers($_SESSION['userID']);

if (isset($_REQUEST['successful'])) {
  echo parseAlert('<strong>Eureka!</strong> Deine Daten wurden erfolgreich bearbeitet!', 'success', true);
}
?>

<input id="contentTitle" type="hidden" data-title="edit/profile/data"/>

<div class="card">
  <div class="card-header">
    <i class="fas fa-user-circle"></i> Deine Daten
    <button id="profileEditButton" class="btn btn-outline-primary float-right" onclick="changeInputs()"
            data-toggle="tooltip" data-placement="right" title="Editiere deine Daten">
      <i class="fas fa-edit"></i>
    </button>

    <button id="profileSaveButton" class="btn btn-outline-success float-right" hidden style="margin-right: 10px;"
            onclick="editUserDataByHimself(this)">
      Speichern
    </button>

  </div>
  <div class="card-body">

    <div class="form-row">
      <div class="col-6 col-sm-6 col-md-3 col-lg-2 col-xl-2">
        <div class="form-group">
          <label for="inputTitle">Titel*</label>
          <div class="input-group" id="inputGroupTitle">
            <div class="input-group-prepend">
              <label for="inputTitle" class="input-group-text"><i class="fas fa-chess-king"></i></label>
            </div>
            <input type="text" class="form-control inputBox" id="inputTitle" name="inputTitle"
                   value="<?php echo $user['title']; ?>" disabled>
          </div>
        </div>
      </div>

      <div class="col-6 col-sm-6 col-md-9 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputFirstname">Vorname*</label>
          <div class="input-group" id="inputGroupFirstname">
            <div class="input-group-prepend">
              <label for="inputFirstname" class="input-group-text"><i class="fas fa-user"></i></label>
            </div>
            <input type="text" class="form-control inputBox" id="inputFirstname" name="inputFirstname"
                   value="<?php echo $user['firstname']; ?>" disabled required oninput="userDataChange(this)">
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
        <div class="form-group">
          <label for="inputSurname">Nachname*</label>
          <div class="input-group" id="inputGroupSurname">
            <div class="input-group-prepend">
              <label for="inputSurname" class="input-group-text"><i class="fas fa-user"></i></label>
            </div>
            <input type="text" class="form-control inputBox" id="inputSurname" name="inputSurname"
                   value="<?php echo $user['surname']; ?>" disabled required oninput="userDataChange(this)">
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
        <div class="form-group">
          <label for="inputBirthdate">Geburtsdatum*</label>
          <div class="input-group" id="inputGroupBirthdate">
            <div class="input-group-prepend">
              <label for="oldValue_inputSurname" class="input-group-text"><i
                  class="fas fa-birthday-cake"></i></label>
            </div>
            <input type="date" class="form-control inputBox" id="inputBirthdate" name="inputBirthdate"
                   value="<?php echo $user['birthdate']; ?>" disabled required oninput="userDataChange(this)">
          </div>
        </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-12 col-sm-10 col-md-10 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputStreetname">Straße</label>
          <div class="input-group" id="inputGroupStreetname">
            <div class="input-group-prepend">
              <label for="inputStreetname" class="input-group-text"><i class="fas fa-road"></i></label>
            </div>
            <input type="text" class="form-control inputBox" id="inputStreetname" name="inputStreetname"
                   value="<?php echo $user['streetname']; ?>" disabled oninput="userDataChange(this)">
          </div>
        </div>
      </div>

      <div class="col-6 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="form-group">
          <label for="inputStreetnumber">Nummer</label>
          <input type="text" class="form-control inputBox" id="inputStreetnumber" name="inputStreetnumber"
                 value="<?php echo $user['streetnumber']; ?>" disabled oninput="userDataChange(this)">

        </div>
      </div>

      <div class="col-6 col-sm-6 col-md-6 col-lg-2 col-xl-2">
        <div class="form-group">
          <label for="inputZipcode">Postleitzahl</label>
          <input type="number" class="form-control inputBox" id="inputZipcode" name="inputZipcode"
                 value="<?php
                 if (isset($userdata['zip_code']) AND !empty($userdata['zip_code'])) {
                   echo $user['zip_code'];
                 } else {
                   echo "1234";
                 }
                 ?>" disabled oninput="userDataChange(this)"
                 onchange="checkZipCode(this)">

        </div>
      </div>

      <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputLocation">Ort</label>
          <div class="input-group" id="inputGroupLocation">
            <div class="input-group-prepend">
              <label for="inputLocation" class="input-group-text"><i class="fab fa-fort-awesome"></i></label>
            </div>
            <input type="text" class="form-control inputBox" id="inputLocation" name="inputLocation"
                   value="<?php echo $user['location']; ?>" disabled oninput="userDataChange(this)">
          </div>
        </div>
      </div>
    </div>

    <div id="oldValue_inputTitle" hidden><?php echo $user['title']; ?></div>
    <div id="oldValue_inputFirstname" hidden><?php echo $user['firstname']; ?></div>
    <div id="oldValue_inputSurname" hidden><?php echo $user['surname']; ?></div>
    <div id="oldValue_inputBirthdate" hidden><?php echo $user['birthdate']; ?></div>
    <div id="oldValue_inputStreetname" hidden><?php echo $user['streetname']; ?></div>
    <div id="oldValue_inputStreetnumber" hidden><?php echo $user['streetnumber']; ?></div>
    <div id="oldValue_inputZipcode" hidden><?php echo $user['zip_code']; ?></div>
    <div id="oldValue_inputLocation" hidden><?php echo $user['location']; ?></div>


    <div class="form-row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <small>Alle Informationen welche in Felder mit * stehen sind für andere DatePoll Benutzer sichtbar.
        </small>
      </div>
    </div>

  </div>
</div>