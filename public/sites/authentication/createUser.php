<form>
	<div class="form-group">
		<label for="createUserEmail">Email address</label>
		<input type="email" class="form-control" id="createUserEmail" aria-describedby="emailHelp"
		       placeholder="Enter email">
		<div class="row">
			<div class="col">
				<label for="createUserPassword1">Password</label>
				<input type="password" class="form-control" id="createUserPassword1" placeholder="Password">
			</div>
			<div class="col">
				<label for="createUserPassword2">Re-enter Password</label>
				<input type="password" class="form-control" id="createUserPassword2" placeholder="Password">
			</div>
		</div>
	</div>

	<div class="form-group">

		<label for="createUserTitle">Titel</label>
		<input type="text" class="form-control" id="createUserTitle" aria-describedby="titleHelp"
		       placeholder="z.B. Dr, MoD, Ing, ...">

		<div class="row">
			<div class="col">
				<label for="createUserFirstname">Vorname</label>
				<input type="text" class="form-control" id="createUserFirstname" aria-describedby="firstnameHelp"
				       placeholder="Vorname">
			</div>
			<div class="col">
				<label for="createUserSurname">Nachname</label>
				<input type="text" class="form-control" id="createUserSurname" aria-describedby="surnameHelp"
				       placeholder="Nachname">
			</div>
		</div>

		<label for="createUserBirthDate">Geburtsdatum</label>
		<input type="date" class="form-control" id="createUserBirthDate" aria-describedby="birthdateHelp"
		       placeholder="01.01.2000">
	</div>

	<div class="form-group">
		<div class="row">
			<div class="col">
				<label for="createUserStreetname">Straße</label>
				<input type="text" class="form-control" id="createUserStreetname" aria-describedby="streetnameHelp"
				       placeholder="Straße">

				<label for="createUserStreetnumber">Hausnummer</label>
				<input type="text" class="form-control" id="createUserStreetnumber" aria-describedby="streetnumberHelp"
				       placeholder="Hausnummer">
			</div>
		</div>

		<div class="row">
			<div class="col">
				<label for="createUserZipCode">Postleitzahl</label>
				<input type="number" class="form-control" id="createUserZipCode" aria-describedby="zipcodeHelp"
				       placeholder="Postleitzahl" minlength="4" maxlength="5">
			</div>
			<div class="col">
				<label for="createUserLocation">Ort</label>
				<input type="text" class="form-control" id="createUserLocation" aria-describedby="locationHelp"
				       placeholder="Ort">
			</div>
		</div>
	</div>

	<div class="form-check">
		<input type="checkbox" class="form-check-input" id="exampleCheck1">
		<label class="form-check-label" for="exampleCheck1">Check me out</label>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>