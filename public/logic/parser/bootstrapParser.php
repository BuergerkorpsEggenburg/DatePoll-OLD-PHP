<?php
/**
 * parses an Bootstrap-card-deck with the given arguments
 * @param string $content the content of the card-deck
 * @param string|null $class optional additional classes for the card-deck
 * @param string|null $attributes optional attributes of the card-deck
 * @return string                  the parsed card-deck
 */
function parseCardDeck($content, $class = null, $attributes = null) {
	return parseTag("div", $content, "class='card-deck $class' " . $attributes);
}

/**
 * parses an Bootstrap-card with the given arguments
 * @param string $header the card-header of the card
 * @param string $body the card-body of the card
 * @param string $footer the card-footer of the card
 * @param string|null $class optional additional classes of the card
 * @param string|null $attributes optional attributes of the card
 * @return string                  the parsed card
 */
function parseCard($header, $body, $footer, $class = null, $attributes = null) {
	return parseTag("div", "$header\n$body\n$footer", "class='card $class' " . $attributes);
}

/**
 * parses an card-header for a Bootstrap-card with the given arguments
 * @param string $content the content of the card-header
 * @param string|null $class optional additional classes of the card-header
 * @param string|null $attributes optional attributes of the card-header
 * @return string                  the parsed card-header
 */
function parseCardHeader($content, $class = null, $attributes = null) {
	return parseTag("div", $content, "class='card-header $class' " . $attributes);
}

/**
 * parses an card-body for a Bootstrap-card with the given arguments
 * @param string $content the content of the card-body
 * @param string|null $class optional additional classes of the card-body
 * @param string|null $attributes optional attributes of the card-body
 * @return string                  the parsed card-body
 */
function parseCardBody($content, $class = null, $attributes = null) {
	return parseTag("div", $content, "class='card-body $class' " . $attributes);
}

/**
 * parses an card-footer for a Bootstrap-card with the given arguments
 * @param string $content the content of the card-footer
 * @param string|null $class optional additional classes of the card-footer
 * @param string|null $attributes optional attributes of the card-footer
 * @return string                  the parsed card-footer
 */
function parseCardFooter($content, $class = null, $attributes = null) {
	return parseTag("div", $content, "class='card-footer $class' " . $attributes);
}

/**
 * parses an card-title for a Bootstrap-card-header with the given arguments
 * @param string $content the content of the card-title
 * @param string|null $class optional additional classes of the card-title
 * @param string|null $attributes optional attributes of the card-title
 * @return string                  the parsed card-title
 */
function parseCardTitle($content, $class = null, $attributes = null) {
	return parseTag("h5", $content, "class='card-title $class' " . $attributes);
}

/**
 * parses an card-subtitle for a Bootstrap-card-header with the given arguments
 * @param string $content the content of the card-subtitle
 * @param string|null $class optional additional classes of the card-subtitle
 * @param string|null $attributes optional attributes of the card-subtitle
 * @return string                  the parsed card-subtitle
 */
function parseCardSubtitle($content, $class = null, $attributes = null) {
	return parseTag("h6", $content, "class='card-subtitle $class' " . $attributes);
}

/**
 * Parses the given content to an 'accordion' like the on of the bootstrap-example-page
 * @param $content  array like [['id' => id, 'header' => heading, 'body' => body, 'class' => classes], ...] Notice: 'class' is optional
 * @return string  array parsed to html
 */
function parseAccordion($content) {
	$accordion = '';
	foreach ($content as $card) {
		$accordion .= '<div class="col-12" style="padding: 0; margin: 0;">' .
				parseCard(
						parseCardHeader(
								parseDiv($card['header'],
										'm-0 p-0 defaultAccordionStyle',
										'data-toggle="collapse" data-target="#genericcollapse' . $card['id'] . '" aria-expanded="false" aria-controls="collapse' . $card['id'] . '"')
								, 'p-0 d-flex align-items-center', 'id="genericheading' . $card['id'] . '"'),
						parseTag('div',
								parseCardBody($card['body']),
								'class="collapse" id="genericcollapse' . $card['id'] . '" aria-labelledby="genericheading' . $card['id'] . '" data-parent="#genericAccordion"',
								''),
						'', (isset($card['class']) ? $card['class'] : ''))
				. '</div>';
	}

	return parseCardDeck($accordion, '', 'id="genericAccordion"');
}

/**
 * parses an Bootstrap-alert with the given arguments
 * @param string $content the content of the alert
 * @param string $type the type of the alert (e.g. danger, success, warning, ...)
 * @param bool $dismissable if true, the user can close the alert by clicking the X
 * @param string $style style which should be applied to alert
 * @return string            the parsed modal
 */
function parseAlert($content, $type = 'success', $dismissable = true, $style = "") {
	return parseTag("div", $content . "\n" .
			($dismissable ? '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<i class="fas fa-times"></i>
												</button>' : ''),
			"class='alert fade show alert-$type " . ($dismissable ? "alert-dismissible" : "") . "' role='alert' style='" . $style . "''");
}

/**
 * contains the defautl-alerts for the js-functions showSuccessAlert and showErrorAlert
 * @return string	the defautl-alerts
 */
function parseStandardAlerts() {
  return '
    <div class="collapse" id="errorAlertCollapse">
      <div class="alert alert-danger alert-dismissible" hidden id="errorAlert" role="alert">
        <strong>Fehler!&nbsp;</strong><div id="errorAlertText"></div>
      </div>
    </div>

    <div class="collapse" id="successAlertCollapse">
      <div class="alert alert-success alert-dismissible" hidden id="successAlert" role="alert">
        <strong>Heureka!&nbsp;</strong><div id="successAlertText"></div>
      </div>
    </div>';
}

function parseStandardModalAlerts() {
  return '
    <div class="collapse" id="modalErrorAlertCollapse">
      <div class="alert alert-danger alert-dismissible" hidden id="modalErrorAlert" role="alert">
        <strong>Fehler!&nbsp;</strong><div id="modalErrorAlertText"></div>
      </div>
    </div>
    <div class="collapse" id="modalSuccessAlertCollapse">
      <div class="alert alert-success alert-dismissible" hidden id="modalSuccessAlert" role="alert">
        <strong>Heureka!&nbsp;</strong><div id="modalSuccessAlertText"></div>
      </div>
    </div>';
}

/**
 * parses an Bootstrap-modal with the given arguments
 * @param string 			$title 		title of the modal
 * @param string 			$content 	the content for the body-section of the modal
 * @param string 			$action 	the content for the footer of the modal
 * @param string|null $size			the size of the modal (large: 'lg', small: 'sm')
 * @return string          			the parsed modal
 */
function parseModal($title, $content, $action, $size = null) {
	$temp = '<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="DatePollModalTitle" aria-hidden="true">
	<div class="modal-dialog '.(isset($size) ? 'modal-'.$size : '').'" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title w-100" id="DatePollModalTitle">' . $title . '</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				' . $content . '
				<input type="hidden" id="action" value="' . $action . '">
			</div>
		</div>
	</div>
</div>';

	return $temp;
}

/**
 * parses a Bootstrap-badge with the given arguments
 * @param string 			$content		the content of the badge
 * @param null|string $class			additional calsses to add to the badge
 * @param null|string $attribute	additional attributes to add to the badge
 * @return string									the parsed badge
 */
function parseBadge($content, $class = null, $attribute = null){
	return parseTag('span', $content, 'class="float-right badge '.$class.'"' . $attribute);
}

?>