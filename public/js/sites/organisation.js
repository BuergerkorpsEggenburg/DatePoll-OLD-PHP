/**
 * loads the modal for editing an organisation
 */
function editOrganisation() {
  load_modal("sites/basic/editOrg.php", 'modalContainer');
}

/**
 * submits the edit dialog
 * @param button    the submit-button of the modal
 */
function editOrganisationSubmit(button) {
  var form = button.parentElement.getElementsByTagName('input');
  var name = form[0].value;
  var description = form[1].value;
  var homepageLink = form[2].value;
  var foundingDate = form[3].value;
  var action = form[4].value;

  var cfunc = function (xhttp) {
    //console.log(xhttp.responseText);
    if (xhttp.responseText == 'true'){
    	load_main_content_and_change_active('./sites/basic/organisation.php', 'organisation');
    	showSuccessAlert("Änderungen gespeichert!");
    }else{
    	showErrorAlert("Es ist ein Fehler aufgetreten beim Bearbeiten!");
    }
  };

  xhr_post('../logic/organisationHandler.php', cfunc, 'action=' + action +
    '&name=' + name +
    '&description=' + description +
    '&homepageLink=' + homepageLink +
    '&foundingDate=' + foundingDate);
}