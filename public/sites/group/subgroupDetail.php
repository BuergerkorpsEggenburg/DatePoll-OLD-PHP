<?php
require_once ('../../logic/groupHandler.php');
require_once ('../../logic/parser.php');

if(!isset($_REQUEST['groupID'])){
	return 'false';
}

$groups = getGroupsAndSubgroups();
$output = array();

foreach ($groups as $subgroup)
	if($subgroup['groupID'] == $_REQUEST['groupID']){
		array_push($output, ['id' => $subgroup['subgroupID'],
				'header' => parseDiv($subgroup['name'], 'p-2 text-warning  w-100'),
				'body' => getMembersOfSubgroupAsTable($subgroup['subgroupID'])
		]);
	}
	$btnAdd = parseTag('button', '<i class="fas fa-plus"></i>', 'class="btn btn-success float-right" onclick="addSubgroup('.$_REQUEST['groupID'].')"');
	$output = parseModal(getGroups($_REQUEST['groupID'])['name'] . $btnAdd , parseAccordion($output), '');

echo $output;