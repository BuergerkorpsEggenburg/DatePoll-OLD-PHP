<?php
require_once('../../logic/userHandler.php');
require_once('../../logic/parser.php');
require_once ('../../logic/settingsHandler.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}
if (!getPermissions()['isAdmin'] == 1) {
	return false;
	die();
}

$settings = getSettings();
?>
<input id="contentTitle" type="hidden" data-title="Settings"/>

<div class="card-group">
	<div class="card">
		<div class="card-header" style="text-align: center;">
			<h1 class="card-title" style="text-align: center;"><i class="fas fa-cog"></i> Einstellungen</h1>
		</div>
		<div class="card-body">
			<div class="form-row">

				<div class="col-12 col-md-6">
					<div class="form-group">
						<label for="settingsDBHost">Datenbank-Server-Addresse:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<span>URL</span>
							</div>
							<input type="text" id="settingsDBHost" placeholder="localhost" class="form-control" value="<?php echo $settings['dbHost']; ?>">
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="form-group">
						<label for="settingsDBName">Datenbank-Name:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<i class="fas fa-database"></i>
							</div>
							<input type="text" id="settingsDBName" placeholder="datepoll" class="form-control" value="<?php echo $settings['dbName']; ?>">
						</div>
					</div>
				</div>

			</div>
			<div class="form-row">

				<div class="col-12 col-md-6">
					<div class="form-group">
						<label for="settingsDBUser">Datenbank-Benutzer:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<i class="fas fa-user"></i>
							</div>
							<input type="text" id="settingsDBUser" placeholder="datepollDBuser" class="form-control" value="<?php echo $settings['dbUser']; ?>">
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="form-group">
						<label for="settingsDBPass">Datenbank-Passwort:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<i class="fas fa-unlock"></i>
							</div>
							<input type="password" id="settingsDBPass" placeholder="1234abc" class="form-control" value="<?php echo $settings['dbPassword']; ?>">
						</div>
					</div>
				</div>

			</div>

			<hr />

			<div class="form-row">

				<div class="col-12 col-md-6">
					<div class="form-group">
						<label for="settingsMailHost">Email-Host:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								URL
							</div>
							<input type="text" id="settingsMailHost" placeholder="datepollmailhost.net" class="form-control" value="<?php echo $settings['mailHost']; ?>">
						</div>
					</div>
				</div>

				<div class="col-12 col-md-3">
					<div class="form-group">
						<label for="settingsMailUser">Email-Benutzer:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<i class="fas fa-user"></i>
							</div>
							<input type="text" id="settingsMailUser" placeholder="DatepollMailUser" class="form-control" value="<?php echo $settings['mailUser']; ?>">
						</div>
					</div>
				</div>

				<div class="col-12 col-md-3">
					<div class="form-group">
						<label for="settingsMailPass">Email-Passwort:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<i class="fas fa-unlock"></i>
							</div>
							<input type="password" id="settingsMailPass" placeholder="1234abc" class="form-control" value="<?php echo $settings['mailPassword']; ?>">
						</div>
					</div>
				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-md-3">
					<div class="form-group">
						<label for="settingsMailPass">Email-Port:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								#
							</div>
							<input type="number" id="settingsMailPort" placeholder="465" class="form-control" value="<?php echo $settings['mailPort']; ?>">
						</div>
					</div>
				</div>

				<div class="col-12 col-md-3">
					<div class="form-group">
						<label for="settingsMailEncryption">Email-Verschlüsselungs-Protokoll:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<span class="text-success">s</span>://
							</div>
							<input type="text" id="settingsMailEncryption" placeholder="ssl" class="form-control" value="<?php echo $settings['mailEncryption']; ?>">
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="form-group">
						<label for="settingsMailContact">Kontakt-Email-Addresse:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								@
							</div>
							<input type="email" id="settingsMailContact" placeholder="support@datepollmailhost.net" class="form-control" value="<?php echo $settings['mailContactAddress']; ?>">
						</div>
					</div>
				</div>

			</div>

			<hr />

			<div class="form-row">

				<div class="col-12">
					<div class="form-group">
						<label for="settingsUserPW">Standard-Passwort für vom Admin angelegte Benutzer:</label>
						<div class="input-group">
							<div class="input-group-prepend input-group-text">
								<i class="fas fa-unlock"></i>
							</div>
							<input type="text" id="settingsUserPW" placeholder="DefaultPw@DatePoll!" class="form-control" value="<?php echo $settings['userDefaultPassword']; ?>">
						</div>
					</div>
				</div>

			</div>

			<button type="button" class="btn btn-primary" onclick="saveSettings()">
				<i class="fas fa-save"></i> Speichern
			</button>
		</div>
	</div>

</div>