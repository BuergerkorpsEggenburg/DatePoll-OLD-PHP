<?php

require_once(__DIR__ . '/../dbConnector.php');

if (isset($_REQUEST['action'])) {
  switch ($_REQUEST['action']) {
    case 'deleteToken':
      $token = $_REQUEST['token'];
      $res = deleteToken($token);
      break;
  }

  echo $res;
}

/**
 * creates a token for stay logged in
 * @param int $userID int the owner of the token
 * @param string $createdDate string the date when the token was created
 * @param string $lastUsedTokenDate  date when the token was last used
 * @param string $browserName name of the browser
 * @param string $browserVersion version of the browser
 * @param string $userAgent useragent of the browser
 * @param string $operatingsystem operating system the browser was used on
 * @return string returns the new generated token
 */
function createToken($userID, $createdDate, $lastUsedTokenDate, $browserName, $browserVersion, $userAgent, $operatingsystem) {
  $conn = connect();

  //Check if token is unique
  while(true) {
    $token = generateRandomString(512);

    if(!tokenExists($token))
      break;
  }

  $stmt = $conn->prepare('INSERT INTO tokens (userID, token, createdDate, lastUsedTokenDate, browserName, browserVersion, userAgent, operatingsystem) VALUES(:userID, :token, :createdDate, :lastUsedTokenDate, :browserName, :browserVersion, :userAgent, :operatingsystem);');
  $stmt->bindParam(':userID', $userID);
  $stmt->bindParam(':token', $token);
  $stmt->bindParam(':createdDate', $createdDate);
  $stmt->bindParam(':lastUsedTokenDate', $lastUsedTokenDate);
  $stmt->bindParam(':browserName', $browserName);
  $stmt->bindParam(':browserVersion', $browserVersion);
  $stmt->bindParam(':userAgent', $userAgent);
  $stmt->bindParam(':operatingsystem', $operatingsystem);

  $stmt->execute();

  return $token;
}


/**
 * deletes a token
 * @param $token string token which should be deleted
 * @return bool return true when everything went good
 */
function deleteToken($token) {
  if(!tokenExists($token)) {
    return false;
  }

  $conn = connect();
  $stmt = $conn->prepare('DELETE FROM tokens WHERE token=:token;');
  $stmt->bindParam(':token', $token);

  $stmt->execute();
  return true;
}

/**
 * check if token is right
 * @param $userID int user who should own the token
 * @param $token string token which should be owned by user
 * @return bool return true if the user owns this token
 */
function checkToken($userID, $token) {
  if(!tokenExists($token)) {
    return false;
  }

  $conn = connect();

  $stmt = $conn->prepare('SELECT token FROM tokens WHERE token = :token AND userID=:userID;');
  $stmt->bindParam(':token', $token);
  $stmt->bindParam(':userID', $userID);

  $stmt->execute();

  if (count($stmt->fetchAll()) > 0)
    return true;
  else
    return false;
}

/**
 * checks if token exists
 * @param $token string token which should be checked if exists
 * @return bool returns true if token exists
 */
function tokenExists($token) {
  $conn = connect();

  $stmt = $conn->prepare('SELECT token FROM tokens WHERE token = :token;');
  $stmt->bindParam(':token', $token);

  $stmt->execute();

  if (count($stmt->fetchAll()) > 0)
    return true;
  else
    return false;
}

/**
 * generate a random string
 * @param int $length how long the token should be
 * @return string returns the new generated string
 */
function generateRandomString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}
/**
 * list all tokens of the given user
 * @param int $userID userID of the user
 * @return array all data from the table
 */
function getAllTokens($userID) {
  $conn = connect();

  $stmt = $conn->prepare("SELECT userID, token, createdDate, lastUsedTokenDate, browserName, browserVersion, userAgent, operatingSystem from tokens WHERE userID = :userID");
  $stmt->bindParam(':userID', $userID);

  $stmt->execute();

  $stmt = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $stmt;
}